<script src="<?=base_url()?>comm/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=base_url()?>comm/css/jquery-ui.min.css">
<link rel="shortcut icon" href="<?=HOSTURL."comm/upload/images/"?>favicon.ico" type="image/x-icon">
<script>
$(function() {
	// 自动完成
	$("#inp-query").autocomplete({
		source: "<?=base_url()?>"+"ajax/search_all",
		delay: 40
	});
	
	// 关注作者
	$("#change-user-follow-status").hover( function() {
		var followed= $(this).attr('followed');
		// alert( type );
		if ( parseInt( followed ) == 1 ) { 
			$(this).text( '取消关注' );
		}
	},  function() {
		var followed= $(this).attr('followed');
		// alert( type );
		if ( parseInt( followed ) == 1 ) {
			$(this).text( '已关注' );
		}
	});
	
	$("#change-user-follow-status").click( function() {
		var type = $(this).attr( 'type' );
		var action = $(this).attr( 'followed' );
		var id = $(this).attr( 'rel' );
		// alert( type );
		$.ajax({
			url: "<?=base_url()?>ajax/follow?type="+type+"&action="+action+"&id="+id,
			success: function( data ) {
				if( action == 0 ) {
					// alert( '关注成功' );
					$("#change-user-follow-status").attr( 'followed', 1 );
					$("#change-user-follow-status").text('已关注');
					$("#change-user-follow-status").parent().addClass( 'followed' );	//移除关注类
					$.ajax({
						url: "<?=base_url()."ajax/follow_notice?from=".$uid."&to="?>"+id
					});
				} else {
					$("#change-user-follow-status").attr( 'followed', 0 );
					$("#change-user-follow-status").text('加关注');
					$("#change-user-follow-status").parent().removeClass( 'followed' );	//移除关注类
				}
			},
			beforeSend: function() {
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		});
	} );
	
	
	// 关注专栏
	$("#change-column-follow-status").hover( function() {
		var followed= $(this).attr('followed');
		// alert( type );
		if ( parseInt( followed ) == 1 ) { 
			$(this).text( '取消关注' );
		}
	},  function() {
		var followed= $(this).attr('followed');
		// alert( type );
		if ( parseInt( followed ) == 1 ) {
			$(this).text( '已关注' );
		}
	});
	
	$("#change-column-follow-status").click( function() {
		var type = $(this).attr( 'type' );
		var action = $(this).attr( 'followed' );
		var id = $(this).attr( 'rel' );
		// alert( type );	// tbd
		$.ajax({
			url: "<?=base_url()?>ajax/follow?type="+type+"&action="+action+"&id="+id,
			success: function( data ) {
				if( action == 0 ) {
					// alert( '关注成功' );
					$("#change-column-follow-status").attr( 'followed', 1 );
					$("#change-column-follow-status").text('已关注');
					$("#change-column-follow-status").parent().addClass( 'followed' );	//移除关注类
				} else {
					$("#change-column-follow-status").attr( 'followed', 0 );
					$("#change-column-follow-status").text('加关注');
					$("#change-column-follow-status").parent().removeClass( 'followed' );	//移除关注类
				}
			},
			beforeSend: function() {
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		});
	} );
	
})
</script>
</head>
	<body>
		<div id="loading"><img src="<?=HOSTURL."comm/upload/images/loading.gif"?>"/></div>
		<div id="header">
			<div class="primary-wrap">
				<div class="navbar primary-box">
					<div class="primary-item logo">
						<a href="<?=HOSTURL?>"><img src="<?=HOSTURL."comm/upload/images/logo.png"?>" alt="故事布丁" width="120px"></a>
					</div>
					
					<div class="primary-item search-box">
						<form action="<?=base_url()?>home/search" method="get">
							<ul>
								<li class="input"><input type="text" id="inp-query" name="key" size="22"  value="" autocomplete="off"/></li>
								<li class="go"><input type="submit" id="btn-submit" value="搜索" /></li>
							</ul>
						</form>
					</div>
					<?php
					function html_not_login() {
						$reg_url = base_url()."user/register";
						$login_url = base_url()."user/login";
					?>
					<div class="primary-item  reg-login pull-right">
						<div class="btn-group">
								<a href="<?=$reg_url?>" class="btn btn-warning btn-sm">注册</a>
								<a href="<?=$login_url?>" class="btn btn-warning btn-sm">登录</a>
						</div>
					</div>
					<?php
					}
					
					
					
					if( $uid == -1 ) {
						html_not_login();
					} else {
						$this->User_model->html_user_photo($login);
					}
					?>
					
					
					<div class="primary-item nav">
						<ul>
							<li><a href="<?=HOSTURL?>"><i class="glyphicon glyphicon-home"></i>首页</a></li>
							<li><a href="<?=HOSTURL.'home/rands'?>"><i class="glyphicon glyphicon-random"></i>传送门</a></li>
							
							<li class="dropdown"><a href="<?=HOSTURL.'user/follow?type=1'?>"><i class="glyphicon glyphicon-ok"></i>关注</a></li>
							<li><a href="<?=HOSTURL.'user/history'?>"><i class="glyphicon glyphicon-time"></i>读过</a></li>
							
							<li><a href="<?=HOSTURL.'user/top'?>"><i class="glyphicon glyphicon-arrow-up"></i>点击榜</a></li>
						</ul>
						
					</div>
				
				</div>
			</div>
			
			<div class="secondary-wrap">
				<div class="secondary-nav">
					
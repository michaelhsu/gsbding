<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>修改专栏配图</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery-pack.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery.imgareaselect.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills pull-left">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <li><a href="<?=HOSTURL.'user/editc?type=0'?>">创建专栏</a></li>
					</ul>
				</div>
			</div>
		</div>
	
		<div id="main">
			<?php 
			$this->Public_model->display_cut_img_box( $f, 560, 200, 1 );
			?>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>	
	</body>
</html>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>修改专栏配图</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<style>
			
		</style>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills pull-left">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <li><a href="<?=HOSTURL.'user/editc?type=0'?>">创建专栏</a></li>
					  </ul>
				</div>
			</div>
		</div>
	
		<div id="main">
			<div id="left-col">
			<?php
			$this->Column_model->html_column_pic_editor( $uid, $cid );
			?>
			</div>
			<div id="right-col">
				<?php
					$this->Public_model->html_write_link();
					$this->Public_model->html_new_stories($uid,0 ,5);
					$this->Public_model->html_hot_stories($uid, 1, 0 ,5);
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
		</div>
	</body>
</html>


<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>专栏</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <?php if( !empty( $uid ) )  {?>
						 <li><a href="<?=HOSTURL.'user/editc?type=0'?>" target="_blank">创建专栏</a></li>
						 
						 <?php } ?>
					  </ul>
					
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left-col">
				<?php
					$this->Column_model->html_column( $uid, $cid );
				?>
				<div class="public" id="public-wrap">
					<div id="stage">
					<?php
					$this->Column_model->html_column_stories_wrap( 1, $uid ,$cid, 0 );
					?>
					</div>
				</div>
			</div>
			
			<div id="right-col">
				<?php
					$this->Column_model->html_column_followers( $cid, 0 , 5 );
					$this->Public_model->html_write_link();
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
		</div>
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
	</body>
</html>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>编辑专栏</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<script>
		$(function() {
			$("#new-column-form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				rules: {
					column_name: {
						required: true,
						maxlength: 100
					},
					column_desc: {
						required: true,
						maxlength: 140
					}
				},
				messages: {
					column_name: {
						required: "专栏名不能为空！",
						maxlength: "专栏名最多100个字（一个英文字母算一个字）。"
					},
					column_desc: {
						required: "专栏描述不能为空！",
						maxlength: "专栏描述最多140个字（一个英文字母算一个字）。"
					}
				}
			});
		});
		</script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills pull-left">
						<li><a href="<?=HOSTURL?>">最新故事</a></li>
						<li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						<li><a href="#">发现</a></li> 
						<li <?php if($type == 0 ) echo "class='active'"; ?>><a href="<?=HOSTURL.'user/editc'?>">创建专栏</a></li>
					</ul>
					
				</div>
			</div>
		</div>
	
		<div id="main">
			<div id="left-col">
				<?php
				$this->Column_model->html_edit_column( $type, $uid );
				?>
			</div>
			
			<div id="right-col">
				<?php
					$this->Public_model->html_write_link();
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
		</div>
		
		<div id="footer">
	
		</div>
	</body>
</html>


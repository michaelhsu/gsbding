<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>写信</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<script>
		$(function() {
			$("#send-msg-form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				rules: {
					content: {
						required: true,
						maxlength: 1000
					}
				},
				messages: {
					content: {
						required: "信件内容不能为空！",
						maxlength: "信件最多100个字（一个英文字母算一个字）"
					}
				}
			});
		});	
		</script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		$msg_link_0 = HOSTURL."user/msg?type=0";
		$msg_link_1 = HOSTURL."user/msg?type=1";
		$msg_link_2 = HOSTURL."user/msg?type=2";
		$write_msg = HOSTURL."user/write_msg";
		?>		<ul  class="nav nav-pills">
						 <li><a href="<?=$msg_link_0?>">私信</a></li>
						 <li><a href="<?=$msg_link_1?>">通知</a></li>
						 <li><a href="<?=$msg_link_2?>">已发送</a></li>
					</ul>	
				</div>
			</div>
		</div>
		
		<div id="main">
			<?php
			$this->User_model->html_write_msg($receiver, $login, $type);
			?>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
	</body>
</html>
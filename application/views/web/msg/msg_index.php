<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>收件箱</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<style>
		
		</style>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		$active_0 = "";
		$active_1 = "";
		$active_2 = "";
		if( $type == 0 ) {
			$active_0 = "class='active'";
		} else if( $type == 1 ) {
			$active_1 = "class='active'";
		} else if( $type == 2 ) {
			$active_2= "class='active'";
		}
		$msg_link_0 = HOSTURL."user/msg?type=0";
		$msg_link_1 = HOSTURL."user/msg?type=1";
		$msg_link_2 = HOSTURL."user/msg?type=2";
		?>		<ul  class="nav nav-pills">
						 <li <?=$active_0?>><a href="<?=$msg_link_0?>">私信</a></li>
						 <li <?=$active_1?>><a href="<?=$msg_link_1?>">通知</a></li>
						 <li <?=$active_2?>><a href="<?=$msg_link_2?>">已发送</a></li>
					</ul>
					
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left-col">
				<?php
				$this->User_model->html_msgbox( 1, $uid, $type );
				?>
			</div>
		</div>
		
		<div id="footer">
	
		</div>
	</body>
</html>
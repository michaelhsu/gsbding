<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>编辑专栏</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills pull-left">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <li <?php if($type == 0 ) echo "class='active'"; ?>><a href="<?=HOSTURL.'user/editc'?>">创建专栏</a></li>
					  </ul>
					
				</div>
			</div>
		</div>
	
		<div id="main">
			<div id="left-col">
				<?php
				$this->User_model->html_edit_column( $type, $uid );
				?>
			</div>
			
			<div id="right-col">
				<?php
					$this->Public_model->html_write_link();
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
		</div>
		
		<div id="footer">
	
		</div>
	</body>
</html>


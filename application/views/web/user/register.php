 <!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>注册</title>
		<!--
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		-->
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		
		<style>
		body {
			background-color: #fff;
		}
		
		.container {
			width: 960px;
		}
		
		.header {
			border-bottom: 1px solid #e5e5e5;
			width: 100%;
		}
		
		.footer {
			margin-top: 10px;
			color: #777;
		}
		
		.header h3 {
			padding-bottom: 19px;
		}
		
		.bd {
			1border: 1px solid #ccc;
			overflow: hidden;
			width: 900px;
			margin: 0 auto;
			padding: 0;
			min-height: 500px;
			margin-top: 50px;
		}
		
		.register-box {
			width: 550px;
			margin: 0 auto;
			1margin-top: 10px;
		}
		
		.gender {
			line-height: 1.7;
			padding: 6px 0;
			height: 34px;
		}
		
		.birthday select {
			padding: 6px;
			border-radius: 4px;
		}
		
		.location-txt {
			border: 1px solid #ccc;
			padding: 6px 12px;
			border-radius: 4px;
			width: 100px;
			height: 34px;
		}
		
		.nav > li > a {
			position: relative;
			display: block;
			padding: 10px 15px;
		}
		
		</style>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<ul class="nav nav-pills pull-right">
				  <li ><a href="<?=base_url()."user/login"?>">登录</a></li>
				  <li class="active"><a href="#">注册</a></li>
				  <li><a href="#">关于</a></li>
				</ul>
				<h3 class="text-muted">故事布丁</h3>
			</div>
			
			<div class="bd">
				<div class="register-box">
					<form class="form-horizontal" role="form" method="post" action="<?=base_url()."user/register"?>" id="reg_form">
							<div class="form-group">
								<div id="error-place">
									<?php echo validation_errors(); ?>
									<?php
									$login_url = base_url()."user/login";
									if( isset( $error ) ) {
										if ($error == 1) {
										
										} else if ( $error == 2) {
											echo "该邮箱已被注册, 请<a href={$login_url}>登录</a>或者换个邮箱注册。";
										} else if ($error == 3) {
											echo "抱歉，注册失败，如果方便，请再注册一次。";
										}
									}
									?>
								</div>
							</div>

						  <div class="form-group">
							<label for="user_email" class="col-sm-2 control-label">邮箱</label>
							<div class="col-sm-8">
							  <input type="email" class="form-control" id="user_email" name="user_email" autocomplete="off">
							</div>
						  </div>
						  <div class="form-group">
							<label for="user_pass" class="col-sm-2 control-label">密码</label>
							<div class="col-sm-8">
							  <input type="password" class="form-control" id="user_pass" name="user_pass">
							</div>
						  </div>
						  <div class="form-group">
							<label for="confirm_user_pass" class="col-sm-2 control-label">确认密码</label>
							<div class="col-sm-8">
							  <input type="password" class="form-control" id="confirm_user_pass" name="confirm_user_pass">
							</div>
						  </div>
						  <div class="form-group">
							<label for="user_nickname" class="col-sm-2 control-label">名号</label>
							<div class="col-sm-8">
							  <input type="text" class="form-control" id="user_nickname" name="user_nickname">
							</div>
						  </div>
							
							<div class="form-group gender">
								<label for="gender" class="col-sm-2 control-label">性别</label>
								<div class="col-sm-8">
									<label class="radio-inline">
										<input type="radio" name="gender" value="1" checked="checked">男
									</label>
									<label class="radio-inline">
										<input type="radio" name="gender" value="2">女
									</label>
								</div>
							 </div>
							 
							 <div class="form-group birthday">
									<label for="birthday" class="col-sm-2 control-label">生日</label>
									<div class="col-sm-8">
											<select name="year">
												<?php
													$birth_year = 1990;
													$toyear = date('Y');
													echo $toyear;
													for($from = 1900; $from <= $toyear; $from++) {
														if ($from == $birth_year) {
															echo "<option value='{$from}' selected>$from</option>"; 
														} else {
															echo "<option value='{$from}'>$from</option>";
														}
													}
												?>
											</select>&nbsp;年
											
											<select name="month">
												<?php
													for($month = 1; $month <= 12; $month++) {
														if($month == $birth_month) {
															echo "<option value='{$month}' selected>$month</option>";
														}	else {
															echo "<option value='{$month}'>$month</option>";
														}
													}
												?>
											</select>&nbsp;月
											
											<select name="day">
												<?php
													for($day = 1; $day <= 31; $day++) {
														if($day == $birth_day) {
															echo "<option value='{$day}' selected>$day</option>";
														}	else {
															echo "<option value='{$day}'>$day</option>";
														}
														
													}
												?>
											</select>&nbsp;日
									</div>
								</div>
								
								<div class="form-group">
									<label for="location"class="col-sm-2 control-label" >所在地</label>
									<div class="col-sm-10">
										<input type="text" name="province" id="province"  class="location-txt"/>&nbsp;省
										<input type="text" name="city" id="city" class="location-txt" />&nbsp;市
									</div>
								</div>
								
								<div class="form-group">
									<label for="graduate" class="col-sm-2 control-label">毕业院校</label>
									<div class="col-sm-8">
										<input type="text"  class="form-control" name="graduate" id="graduate"/>
									</div>
								</div>
								
								<div class="form-group">
									<label for="workplace" class="col-sm-2 control-label">工作单位</label>
									<div class="col-sm-8">
										<input type="text" name="workplace" id="workplace" class="form-control"/>
									</div>
								</div>
						  
						  
							  <div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-success" name="btn_reg">注&nbsp;&nbsp;册</button>
								</div>
							  </div>
						</form>		  
				</div>
			</div>
			
			<div class="footer">
				<p>&copy;<?=date('Y')?></p>
			</div>
			<script>
			$( function() {
				$("#reg_form").validate({
					errorPlacement: function(error, element){
						$('html,body').animate({
							scrollTop: 0
						}, 500);
						error.appendTo( $("#error-place") );
					}, 
					rules: {
						user_email: {
							required: true,
							email: true
						},
						user_pass: {
							required: true,
							minlength: 6,
							maxlength: 50
						},
						confirm_user_pass: {
							required: true,
							minlength: 6,
							maxlength: 50,
							equalTo: "#user_pass"
						},
						user_nickname: "required"
					},
					messages: {
						user_email: {
							required: "请输入email地址。",
							email: "请输入合法的email地址"
						},
						user_pass: {
							required: "请输入密码",
							minlength: "密码的最小长度为6。",
							maxlength: "密码的最大长度为50"
						},
						confirm_user_pass: {
							required: "请输入确认密码",
							minlength: "确认密码的最小长度为6。",
							maxlength: "确认密码的最大长度为50",
							equalTo: "两次输入的密码不一致。"
						},
						user_nickname: "请输入您的名号。"
					}
				});
			
			} ); 
			</script>
	</div>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>修改密码</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<script>
		$(function() {
			$("#chk-pass-form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				rules: {
					old_pass: {
						required: true,
						minlength: 6,
						maxlength: 50
					},
					new_pass: {
						required: true,
						minlength: 6,
						maxlength: 50
					},
					new_pass2: {
							required: true,
							minlength: 6,
							maxlength: 50,
							equalTo: "#new_pass"
					}
				 },
				messages: {
					old_pass: {
					required: "请输入原密码。",
					minlength:"密码长度至少为6个字符。",
					maxlength: "密码长度至多为50个字符。"
					},
					new_pass: {
					required: "请输入新密码。",
					minlength:"密码长度至少为6个字符。",
					maxlength: "密码长度至多为50个字符。"
					},
					new_pass2: {
					required: "请确认新密码。",
					minlength:"密码长度至少为6个字符。",
					maxlength: "密码长度至多为50个字符。",
					equalTo: "两次输入密码不一致"
					}
				}
			});
		});	
		</script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul  class="nav nav-pills">
						 <li><a href="<?=HOSTURL."user"?>">本人资料</a></li>
						 <li><a href="<?=HOSTURL."user/editp?type=0"?>" >修改资料</a></li>
						 <li><a href="<?=HOSTURL."user/editp?type=1"?>">修改头像</a></li>
						 <li class="active"><a href="<?=HOSTURL."user/editp?type=2"?>">修改密码</a></li>
					</ul>
					
				</div>
			</div>
		</div>
	
		<div id="main">
			<div id="left-col">
				<?php
				$this->User_model->html_password_editor( $login );
				?>
			</div>
			
			<div id="right-col">
				<?php
					$this->Public_model->html_write_link();
					$this->Public_model->html_new_stories($uid,0 ,5);
					$this->Public_model->html_hot_stories($uid, 1, 0 ,5);
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
		</div>
		
		<div id="footer">
	
		</div>
	</body>
</html>


<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>评论主页</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>	
					<ul  class="nav nav-pills">
						 <li <?php if($uid == $author_id) echo 'class="active"'?>><a href="<?=HOSTURL."user"?>">本人资料</a></li>
						 <li><a href="<?=HOSTURL."user/editp?type=0"?>" >修改资料</a></li>
						 <li><a href="<?=HOSTURL."user/editp?type=1"?>">修改头像</a></li>
						 <li><a href="<?=HOSTURL."user/editp?type=2"?>">修改密码</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left-col">
			<?php
			$start = 0;
			$size = 100;
			$this->User_model->html_user_big_profile( $uid, $author_id );
			
			$user_index_link = base_url()."user/?id=".$author_id;
			$user_comment_link = base_url()."user/user_comment?id={$author_id}";
			$user_vote_link = base_url()."user/user_vote?id={$author_id}";
			$user_draft_link = base_url()."user/user_draft?id={$author_id}";
			?>
				<div class="public" id="public-wrap">
					<div class="switcher">
						<ul class="nav nav-tabs">
							<li><a href="<?=$user_index_link?>">故事</a></li>
							<li class="active"><a href="<?=$user_comment_link ?>">评论</a></li>
							<li><a href="<?=$user_vote_link?>">赞</a></li>
							<?php 
							if( $uid == $author_id ) {
								echo "<li><a href='{$user_draft_link}'>草稿</a></li>";
							}
							?>
						</ul>
					</div>
					
					<div id="stage">
						<?php
						// $this->User_model->html_user_stories_wrap( 1, $uid ,$author_id, 0 );
						?>
					</div>
				</div>
				
			</div>
			
			<div id="right-col">
				<?php
				$this->Public_model->html_write_link();
				$this->User_model->html_follow_list_short( 0, $author_id, 0, 12 );
				$this->User_model->html_follow_list_short( 1, $author_id, 0, 12 );
				$this->User_model->html_author_followers($author_id, 0, 12);
				?>
			</div>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
	</body>
</html>
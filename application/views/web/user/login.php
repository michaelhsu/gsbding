<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>登录</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<style>
		body {
			background-color: #fff;
		}
		
		.container {
			width: 960px;
		}
		
		.header {
			border-bottom: 1px solid #e5e5e5;
		}
		
		.footer {
			color: #777;
		}
		
		.header h3 {
			padding-bottom: 19px;
		}
		
		.bd {
			overflow: hidden;
			width: 900px;
			margin: 0 auto;
			padding: 0;
			min-height: 500px;
			margin-top: 50px;
		}
		
		.login-info-wrap {
			float: left;
			width: 450px;
			margin: 0;
			padding: 0;
		}
		
		.login-wrap {
			border: 1px solid #eee;
			float: left;
			width: 400px;
			margin: 0;
			padding: 0;
			border-radius: 5px;
			margin-top: 40px;
			overflow: hidden;
			background-color: #f2f2f2;
		}
		
		.login-info-wrap .slogan {
			text-align: center;
			letter-spacing: 2px;
		}
		
		.login-wrap .login-box {
			width: 260px;
			margin: 0 auto;
			margin-top: 30px;
			padding: 4px;
		}
		
		.btn-login {
			width: 100%;
			padding: 8px 0;
		}
		
		.nav > li > a {
			position: relative;
			display: block;
			padding: 10px 15px;
		}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<ul class="nav nav-pills pull-right">
				  <li class="active"><a href="#">登录</a></li>
				  <li><a href="<?=base_url()."user/register"?>">注册</a></li>
				  <li><a href="#">关于</a></li>
				</ul>
				<h3 class="text-muted">故事布丁</h3>
			</div>
		
		
			<div class="bd">
				<div class="login-info-wrap">
					<div class="img-wrap">
						&nbsp;
					</div>
					<div class="slogan">
					
					</div>
				</div>
				
				<div class="login-wrap">
					<div class="login-box">
						<form role="form" method="post" action="<?=base_url()."user/login"?>" id="login_form">
							<div class="form-group">
								<div id="error-place">
									<?php echo validation_errors(); ?>
									<?php
									if( isset( $error ) && $error==2 ) {
										echo '密码错误！';
									}
									?>
								</div>
							</div>
						  <div class="form-group">
							<label for="user_email">邮&nbsp;箱</label>
							<input type="email" class="form-control" id="user_email" name="user_email" >
						  </div>
						  <div class="form-group">
							<label for="passwd">密&nbsp;码</label>
							<input type="password" class="form-control" id="user_pass" name="user_pass" >
						  </div>
						  <div class="checkbox">
							<label>
							  <input type="checkbox" name="remember" checked> 记住密码
							</label>
						  </div>
						  <div class="form-group">
							<button type="submit" name="btn_login" class="btn btn-success  btn-login">登&nbsp;&nbsp;&nbsp;&nbsp;录</button>
						  </div>
						</form>
						<ul>
							<li><a id="link-forgot-passwd" href="<?=HOSTURL."/module/msg/note.php"?>" target="_blank" >忘记密码？</a></li>
						</ul>
					</div>
				</div>
			</div>
		
			<div class="footer">
				<p>&copy;<?=date('Y')?></p>
			</div>
		</div>
		<script>
		$(function() {
			$("#login_form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				
				rules: {
					user_email: {
						required: true,
						email: true
					},
					user_pass: {
						required: true,
						minlength: 6,
						maxlength: 50
					}
				},
				messages: {
					user_email: {
						required: "请输入email地址。",
						email: "请输入合法的email地址。"
					},
					user_pass: {
						required: "请输入密码。",
						minlength: "密码的最小长度为6。",
						maxlength: "密码的最大长度为50。"
					}
				}
			});
		} );
		</script>
	</body>
</html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>修改头像</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery-pack.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery.imgareaselect.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul  class="nav nav-pills">
						 <li><a href="<?=HOSTURL."/module/user"?>">本人资料</a></li>
						 <li><a href="<?=HOSTURL."/module/user/edit-profile.php?type=0"?>" >修改资料</a></li>
						 <li class="active"><a href="<?=HOSTURL."/module/user/edit-photo.php"?>">修改头像</a></li>
						 <li><a href="<?=HOSTURL."/module/user/edit-profile.php?type=1"?>">修改密码</a></li>
					</ul>
				</div>
			</div>
		</div>
	
		<div id="main">
			<?php
            //echo print_r($f);
            if (empty($f['name']))
            {
                echo "没有选择图片";
                ?><br /><a href="<?=HOSTURL.'user/editp?type=1'?>">修改头像</a><?php
            }

             else
			    $this->Public_model->display_cut_img_box( $f, 560, 200 );
			?>
		</div>
</body>
</html>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>关注</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul  class="nav nav-pills">
						 <li <?php if( $type == 0 ) echo 'class="active"';?>><a href="<?=HOSTURL."user/follow?type=0"?>">关注专栏</a></li>
						 <li <?php if( $type == 1 ) echo 'class="active"';?>><a href="<?=HOSTURL."user/follow?type=1"?>">关注作者</a></li>
						 <li <?php if( $type == 2 ) echo 'class="active"';?>><a href="<?=HOSTURL."user/follow?type=2"?>">读者</a></li>
						 <li><a href="#" >收藏故事</a></li>
						  <li><a href="#">创建专栏</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left-col">
				<?php
				$this->User_model->html_follow_list( $r );
				?>
			</div>
			
			<div id="right-col">
				<?php
					$this->Public_model->html_write_link();
					$this->Story_model->html_user_short_profile($author_id, $uid );
					
					if( $uid == -1 ) {
					
					} else if( $uid == $author_id ) {	// 是本人的话
						$this->Public_model->html_new_stories($uid,0 ,5);
						$this->Public_model->html_hot_stories($uid, 1, 0 ,5);
					} else {	// 非本人，则推荐专栏和作者
					
					}
					
				?>
			</div>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
		
	</body>
</html>
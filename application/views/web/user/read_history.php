<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>阅读历史</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery-pack.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/image_upload/js/jquery.imgareaselect.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul  class="nav nav-pills">
						 <li <?php if( $type == 0 )  echo " class='active'"?>><a href="<?=HOSTURL."user/history?type=0"?>" >今天</a></li>
						 <li <?php if( $type == 1 )  echo " class='active'"?>><a href="<?=HOSTURL."user/history?type=1"?>" >更早</a></li>
					</ul>
				</div>
			</div>
		</div>
	
		<div id="main">
			<?php 
			$this->Public_model->html_read_history( $type, $history );
			?>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>	
</body>
</html>
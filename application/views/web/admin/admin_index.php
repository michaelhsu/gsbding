<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>管理首页</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<style>
		.primary-box .nav-pills > li > a {
			position: relative;
			display: block;
			padding: 10px 15px;
		}
		
		
		.primary-box .nav-pills > li.active > a  {
			color: #fff !important;
		}
		
		.primary-nav {
			margin-top: -6px;
		}
		</style>
		<?php
		require_once(VIEWPATH. '/web/admin-header.tpl.php');
		$add_story_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea/edit_story";
		$all_stories_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea?type=1";
		$today_stories_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea?type=0";
		$cls0 = '';
		$cls1='';
		if( $type == 0 ) {
			$cls0 = 'active';
		} else if( $type == 1 ) {
			$cls1 = 'active';
		}
		?>	
					<ul  class="nav nav-pills">
						<li class="<?=$cls0?>"><a href="<?=$today_stories_link?>">今日</a></li>
						<li class="<?=$cls1?>"><a href="<?=$all_stories_link?>" >全部</a></li>
						<li><a href="<?=$add_story_link ?>" target="_blank">添加故事</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left_col">
				<?php
				$this->Admin_model->html_stories( $type, 1 );
				?>
			</div>
			
			<div id="right_col">
			
			</div>
		</div>
		
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
	</body>
</html>
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<title>添加故事</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/admin-style.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/themes/default/default.css" />
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/plugins/code/prettify.css" />
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/kindeditor.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/lang/zh_CN.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/plugins/code/prettify.js"></script>
		
		<script>
			var editor;
			KindEditor.options.cssData = 'body { font-size: 13px; } ';
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="story_content"]', {
					resizeType : 0,
					// width: '530px',
					width: '630px',
					height: '400px',
					uploadJson : 'kindeditor/php/upload_json.php',
					fileManagerJson : 'kindeditor/php/file_manager_json.php',
					allowFileManager : false,
					allowPreviewEmoticons : false,
					allowImageUpload : true,
					items : [
						'source', 'removeformat', 'fullscreen', '|', 'fontname', 'fontsize', '|', 'bold', 'italic', 'underline','forecolor', 'hilitecolor', 
						 '|', 'justifyleft', 'justifycenter', 'justifyright', '|','image', 'link' ]
				});
			});
		</script>	
		
		<script>
		$(function() {
			$("#write-form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				rules: {
					story_title: {
						required: true,
						maxlength: 100
					},
					story_content: {
						required: true
					}
				},
				messages: {
					story_title: {
						required: "标题不能为空。",
						maxlength: "标题最多100个字（一个英文字母算一个字）"
					},
					story_content: {
						required: "内容不能为空。"
					}
				}
			});
		});
		</script>
		<?php
		require_once(VIEWPATH. '/web/admin-header.tpl.php');
		$add_story_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea/edit_story";
		$all_stories_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea?type=1";
		$today_stories_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea?type=0";
		?>	
				<ul  class="nav nav-pills">
					<li><a href="<?=$today_stories_link?>">今日</a></li>
					<li><a href="<?=$all_stories_link?>" >全部</a></li>
					<li class="active"><a href="<?=$add_story_link ?>" target="_blank">添加故事</a></li>
				</ul>
				</div>
			</div>
		</div>
	
		<div id="main">	
			<?php
			 $this->Admin_model->html_editor( $act, $story_id );
			?>
		</div>
			
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>	
	
	</body>
</html>

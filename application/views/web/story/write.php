<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<title>创作故事</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/themes/default/default.css" />
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/plugins/code/prettify.css" />
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.validate.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/kindeditor.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/lang/zh_CN.js"></script>
		<script charset="utf-8" src="<?=base_url()?>comm/kindeditor/plugins/code/prettify.js"></script>
		
		<script>
			var editor;
			KindEditor.options.cssData = 'body { font-size: 13px; } ';
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="story_content"]', {
					resizeType : 0,
					// width: '530px',
					width: '630px',
					height: '400px',
					uploadJson : 'kindeditor/php/upload_json.php',
					fileManagerJson : 'kindeditor/php/file_manager_json.php',
					allowFileManager : false,
					allowPreviewEmoticons : false,
					allowImageUpload : true,
					items : [
						'source', 'removeformat', 'fullscreen', '|', 'fontname', 'fontsize', '|', 'bold', 'italic', 'underline','forecolor', 'hilitecolor', 
						 '|', 'justifyleft', 'justifycenter', 'justifyright', '|','image', 'link' ]
				});
			});
		</script>	
		
		<script>
		$(function() {
			$("#write-form").validate({
				errorPlacement: function(error, element){
					$('html,body').animate({
						scrollTop: 0
					}, 500);
					error.appendTo( $("#error-place") );
				}, 
				rules: {
					story_title: {
						required: true,
						maxlength: 100
					},
					story_content: {
						required: true
					}, 
					column_from: "required"
				},
				messages: {
					story_title: {
						required: "标题不能为空。",
						maxlength: "标题最多100个字（一个英文字母算一个字）"
					},
					story_content: {
						required: "内容不能为空。"
					},
					column_from: "请选择一个专栏！专栏选项就在发表按钮上方。"
				}
			});
		});
		</script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li class="active"><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <?php if( !empty( $uid ) )  {?>
						 <li><a href="<?=HOSTURL.'user/editc'?>">创建专栏</a></li>
						 <?php } ?>
					</ul>
				</div>
			</div>
		</div>
	
		<div id="main">	
			<?php
			 $this->Story_model->html_editor($act, $s);
			 $this->Public_model->html_copyright_declare();
			?>
		</div>
			
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>	
	
	</body>
</html>

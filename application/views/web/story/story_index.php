<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="让人们即使在上厕所时也能读到一两个有气质的故事" />
		<meta name="keywords" content="故事布丁 小故事 故事" />
		<title>故事首页</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		
					<ul class="nav nav-pills pull-left">
						 <li class="active"><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <li><a href="<?=HOSTURL.'user/editc?type=0'?>">创建专栏</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="main">
			
			
			<div id="left-col">
				<div class="home-wrap">
					<div class="model-switcher">
						<div class="btn-group">
								<a  href="<?=base_url()?>?mod=0" class="btn btn-success <?php if ($mod == 0 ) echo "active"?>">专栏</a>
								<a href="<?=base_url()?>?mod=1" class="btn btn-success <?php if ($mod == 1 ) echo "active"?>">作者</a>
						</div>
					</div>
					
					<div id="stage">
						<?php 
						$this->Public_model->html_feed_stories_list(1, $uid, $mod, 0);
						?>
					</div>
			</div>
		</div>
		
		<div id="right-col">
				<?php
					//$this->Public_model->html_declare();
					
					$this->Public_model->html_new_comments($uid, 0, 8);
					$this->Public_model->html_write_link();
					$this->Public_model->html_new_votes($uid, 0, 10);
					$this->Public_model->html_new_stories($uid, 0, 10);
					$this->Public_model->html_suggest_authors( 0 , 12 );
					$this->Public_model->html_suggest_columns( 0 , 12 );
				?>
			</div>
	</div>
		
	<?php
	require_once(VIEWPATH. '/web/footer.tpl.php');
	?>	
</body>
</html>
	
<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?=$s['story_title']?></title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>comm/js/user-action.js"></script>
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		<ul class="nav nav-pills">
						 <li><a href="<?=HOSTURL?>">最新故事</a></li>
						 <li ><a href="<?=HOSTURL.'home/write'?>">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <?php if( $uid != -1)  {?>
						 <li><a href="<?=HOSTURL.'user/editc'?>">创建专栏</a></li>
						 
						 <?php } ?>
					  </ul>
					
				</div>
			</div>
		</div>
		
		<div id="main">
			<div id="left-col" class="reddit">
				<div class="eat-bding-wrap">
					<div class="eat-bding-box">
			<?php
				//print_arr( $s );
				$this->Story_model->html_complete_story($uid, $s);
			?>
					</div>
				</div>
			</div>
			
			<div id="right-col">
				<?php
					// $this->Public_model->html_copyright_declare();
					$this->Public_model->html_write_link();
					$this->Story_model->html_user_short_profile($s['user_id'], $uid );
					$this->Story_model->html_short_column($s['topic_id'], $uid );
					$this->Public_model->html_new_stories($uid, 0, 5 );
				?>
			</div>
		</div>
		<?php
		require_once(VIEWPATH. '/web/footer.tpl.php');
		?>
	</body>
</html>
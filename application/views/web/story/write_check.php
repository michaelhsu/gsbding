<!DOCTYPE html>
<html lang="zh-cn">
	<head>
		<meta charset="utf-8" />
		<title>创作故事</title>
		<link rel="stylesheet" href="<?=base_url()?>comm/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/css/style.css">
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/themes/default/default.css" />
		<link rel="stylesheet" href="<?=base_url()?>comm/kindeditor/plugins/code/prettify.css" />
		<script type="text/javascript" src="<?=base_url()?>comm/js/jquery.min.js"></script>
		<script src="<?=base_url()?>comm/js/bootstrap.min.js"></script>
		
		<?php
		require_once(VIEWPATH. '/web/header.tpl.php');
		?>		<ul class="nav nav-pills">
						 <li><a href="#">最新故事</a></li>
						 <li class="active"><a href="#">写故事</a></li>
						 <li><a href="#">发现</a></li> 
						 <?php if( !empty( $uid ) )  {?>
						 <li><a href="#">我的作品</a></li>
						 <li><a href="#">我的草稿</a></li>
						 <li><a href="#>">创建专栏</a></li>
						 <?php } ?>
					</ul>
				</div>
			</div>
		</div>
	
		<div id="main">	
			<div id="left-col">
				<p>数据提交成功</p>
				<?php
				print_arr( $p );
				// redirect();
				?>
			</div>
			
			<div id="right-col">
				
			</div>
	
	</body>
</html>

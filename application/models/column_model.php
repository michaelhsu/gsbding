<?php
class Column_model extends CI_Model {
	var $cid = "";
	var $uid = "";

	public function __construct( $uid="", $cid="" ) {
		$this->uid = $uid;
		$this->cid = $cid;
		$this->load->database();
	}
	
	public function is_empty_cid() {
		return empty( $this->cid );
	}
	
	public function get_column( $id = "") {
		$data['error'] = 0; 	// 默认无错误
		$data['row'] = array();
		if( empty( $id ) ) {
			$id = $this->cid;
		}
		
		
		$sql = "
			SELECT *
			FROM topic 
			WHERE topic_id = {$id}
		";
		
		$query = $this->db->query( $sql );
		$data['row'] = $query->row_array();
		return $data;
	}
	
	function num_column_stories( $cid ) {
		$sql = "
			SELECT COUNT(*) AS num
			FROM story
			WHERE topic_id = '{$cid}'
				AND story_type = '0'
		";
		
		$query = $this->db->query( $sql );
		return $query->row()->num;
	}
	
	function html_column_profile( $column = array() ) {
		// 打印用户的详细资料
		// 
		extract( $column );
		$date = substr( $topic_create_date, 0 ,16 );
		echo "<div class='column-big-title'>";
		echo "<h3>{$topic_title}<i>[专栏]</i ></h3>";
		echo "<p>{$topic_desc}</p>";
		echo "</div>";
		$num = $this->num_column_stories( $topic_id );
		// begin print
		echo '<ul class="column-attach">';
		echo "<li><label>创建时间</label><span>{$date}</span></li>";
		echo "<li><label>作品统计</label><span>{$num}篇</span></li>";
		echo '</ul>';
	}
	
	function html_interact_with_column( $uid, $cid ) {	// tbe
		// 用户与专栏的互动( 已关注 or 未关注 )
		$follow_or_not = $this->User_model->follow_or_not( 0, $uid, $cid );
		// echo $follow_or_not;
		if( $follow_or_not == 0 ) {
			$status = "加关注";
			$cls = "";
		} else if( $follow_or_not == 1 ) {
			$status = "已关注";
			$cls = "followed";
		}
		$write_link = HOSTURL."home/write";
		?>
		<ul>
			<?php
			if( $uid > 9999 ) {
			?>
			<li class="<?=$cls?>"><a href="javascript:void(0);" rel="<?=$cid?>" type="0" followed="<?=$follow_or_not?>" action="follow" id="change-column-follow-status"><?=$status?></a></li>
			<?php
			}
			?>
			<li><a href="<?=$write_link?>" action="" id="add-bding" target="_blank">写故事</a></li>
		</ul>
		<?php
	}
	
	function get_recent_stories( $xid, $start, $size ) {
		// 获取作者最近发布的作品
		// author_id = 作者编号
		// num = 作品数量
		// 返回作品数组
		// type 0=专栏 1=作者
		$data['error'] = 0;
		$data['list'] = array();
		if( empty($xid) ) {
			$data['error'] = 1;
			return $data;
		}
			
		$sql = "SELECT 
				user.user_id AS cat_id,
				user.user_nickname AS cat_name,
				topic.topic_id,
				topic.topic_title,
				user.user_photo AS img,
				story.story_id,
				story.story_title,
				story.story_content,
				story.post_date,
				story.view_count,
				story.user_id
			FROM story,topic,user
			WHERE story.topic_id = '{$xid}' 
			  AND story.topic_id=topic.topic_id
			  AND story.user_id = user.user_id
			  AND story.story_type = '0'
			ORDER BY story.post_date DESC
			LIMIT {$start}, {$size}
		";

	
		$query = $this->db->query( $sql );
		$data['list'] = $query->result_array();
		return $data;
	}
	
	
	function html_column_stories( $uid, $cid, $start, $size) {
		// html最近发布的文章
		$r = $this->get_recent_stories( $cid, $start, $size );
		if( $r['error'] == 0 ) {
			$sl = $r['list'];
		?>
		<script src="<?=base_url().'comm/js/user-action.js'?>"></script>
		<?php
		} else {
			echo '<p>查询错误</p>';
		}
		echo "<div class='column-bdings-wrap' id='bding-feed-list'>";
		echo "<div class='column-bdings-box'>";
		if( count( $sl ) == 0 ) {
			echo "<p>尚未发布作品</p>";
		} else {
			foreach( $sl as $story ) {
				$this->Public_model->html_bding_item_author($uid, $story );
			}
		}
		echo "</div>";
		echo "</div>";
	}
	
	function html_column( $uid, $cid ) {
		$column = $this->get_column( $cid );
		
		$column_data = $column['row'];
		extract( $column_data  );
		// echo CVIEW_PATH;
		$topic_icon = CVIEW_PATH.$topic_icon;
		$edit_desc_link = HOSTURL."column/update_pic?id={$cid}";
		$edit_pic_link = HOSTURL."column/update_pic?id={$cid}";
	?>
		<div class="column-wrap">
			<div class="column-box">
				<div class="column-big-photo">
					<img src="<?=$topic_icon?>" width="200px" height="200px"/>
					<ul class="nav nav-justified">
						<li><a href="<?=$edit_desc_link?>">修改配图</a></li>
						<li><a href="<?=$edit_pic_link?>">修改描述</a></li>
					</ul>
				</div>
				
				<div class="pull-left profile">
					<div class="column-all-profile">
						<?php
						$this->html_column_profile( $column_data  );
						?>
					</div>
					
					<div class="user-action">
						<?php
						// echo $uid;
						$this->html_interact_with_column( $uid, $topic_id );
						?>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
	
	function html_column_stories_wrap( $page, $uid ,$cid, $type ) {
		// 显示专栏下的故事
		$size = PAGE_SIZE;
		$start =( $page - 1 ) * $size;
		$stories_num = $this->num_column_stories( $cid );
		$total_page = ceil( $stories_num / $size );
		if( $stories_num > 0 ) {
	?>
		<div class="display-switcher">
			<a href="javascript:void(0);" action="change-columnp" p="<?=$page?>" type="0" rel="<?=$cid?>"><span class="glyphicon glyphicon-th-large" title="摘要显示"></span></a>
			<a href="javascript:void(0);" action="change-columnp" p="<?=$page?>" type="1" rel="<?=$cid?>"><span class="glyphicon glyphicon-th-list" title="列表显示"></span></a>
		</div>
	<?php
		}
		if( $type == 0 ) {
			$this->html_column_stories( $uid, $cid, $start, $size );
		} else if( $type == 1 ) {
			// 列表显示
			$this->html_column_stories_hoz( $uid, $cid, $start, $size );
		}
		if( $total_page > 1 ) {
		?>
		<div class="page-switcher">
			<?php
				if( $page > 1 ) {
			?>
					<span><a href="javascript:void(0)" action="change-columnp" type="<?=$type?>" id="prev-page" p="<?=$page - 1?>"  rel="<?=$cid?>">上一页</a></span>
			<?php
				}
				echo "<select id='sel-column-page' rel='{$cid}' type='{$type}'>";
				for($i = 1; $i <= $total_page; $i++) {
			?>
					<option value="<?=$i?>" <?php if($page == $i) echo "selected";?>><a href="javascript:void(0)" action="changep" type="<?=$type?>" p="<?=$i?>">第<?=$i?>页</a></option>
			
			<?php
				}
				echo "</select>";
				if( $page < $total_page ) {
			?>
					<span><a href="javascript:void(0)" action="change-columnp" type="<?=$type?>" id="prev-page" p="<?=$page +1?>" rel="<?=$cid?>">下一页</a></span>
			<?php
				}
			?>
		</div>
		<?php
		}
	}
	
	
	function html_column_stories_hoz( $uid, $cid, $start, $size ) {
		// 列表显示
		$r = $this->get_recent_stories( $cid, $start, $size );
		if( $r['error'] == 0 ) {
			$sl = $r['list'];
		?>
		<script src="<?=base_url().'comm/js/user-action.js'?>"></script>
		<?php
		} else {
			echo '<p>查询错误</p>';
		}
		?>
			<div class='column-bdings-wrap-hoz' id='bding-feed-list'>
				<div class='column-bdings-box-hoz'>
			<?php
			if( count( $sl ) == 0 ) {
				echo "<p>尚未发布作品</p>";
			} else {
				echo "<ul>";
				foreach( $sl as $story ) {
					$this->html_column_story_item_hoz($uid, $story );
				}
				echo "</ul>";
			}
			?>
				</div>
			</div>
		<?php
	}
	
	function html_column_story_item_hoz( $uid, $story ) {
		extract( $story );
		// print_arr( $story );
		$user_photo = UVIEW_PATH.$img;
		$user_link = HOSTURL."user/?id=".$cat_id;
		$story_link = HOSTURL."home/reddit?id=".$story_id;
		$s_title = $this->Public_model->story_summary( $story_title, 15 );
		$post_date = substr( $post_date, 0 , 10 );
	?>
		<li id="story-<?=$story_id?>">
			<ul class="story-item-hoz">
				<li class="img"> 
					<a  href="<?=$user_link?>" target="_blank"  title="<?=$cat_name?>" >
					<img class="media-object" src="<?=$user_photo?>" width="30px" height="30px">
					</a>
				</li>
				<li class="post-date"><?=$post_date?></li>
				<li class="story-title"><a href="<?=$story_link ?>" target="_blank" action="eat-bding" rel="<?=$story_id?>" title="<?=$story_title?>"><?=$s_title?></a></li>
				<?php
					if( ($uid != -1) && ($uid == $cat_id ) ) {
						$edit_link = HOSTURL."home/write?act=1&id={$story_id}";
				?>
				<li class="op">
					<span><a href="<?=$edit_link?>"><i class='glyphicon glyphicon-edit' style='margin-right: 5px;'></i>编辑</a></span>&nbsp;
					<span><a href="javascript:void(0);" action="del-bding" rel="<?=$story_id?>"><i class='glyphicon glyphicon-remove-sign' style='margin-right: 5px;'></i>删除</a></span>
				</li>
				<?php
					}
				?>
				<li class="view-count pull-right">阅读(<?=$view_count?>)</li>
				
			</ul>
			
		</li>
	<?php
	}
	
	function get_column_followers( $cid, $start, $size ) {
		$sql = "
			SELECT
				user.user_id,
				user.user_nickname,
				user.user_photo
			FROM user, follow_topic
			WHERE
				follow_topic.topic_id = '{$cid}'
				AND follow_topic.user_id = user.user_id
			ORDER BY follow_topic.follow_date DESC
			LIMIT {$start}, {$size}
		";
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function	html_column_followers( $cid, $start, $size ) {
		$data = $this->get_column_followers( $cid, $start, $size );
		if( count( $data ) > 0 ) {
		?>
			<div class="attach-wrap">
				<div class="title"><span>谁关注了它<i style="color: #0000B3">/ who follow it</i></span></title></div>
				<div class="attach-box">
				<ul>
				<?php
					// print_arr( $data );
					foreach( $data as $item ) {
						extract( $item );
						$user_link = HOSTURL."user/?id=".$user_id;
						$user_photo = UVIEW_PATH.$user_photo
				?>
						<li class="follow-item"><a href="<?=$user_link?>" target="_blank" title="<?=$user_nickname?>" alt="<?=$user_nickname?>"><img src="<?=$user_photo?>" width="50px" height="50px"/></a></li>
				<?php
					}
				?>
				</ul>
				</div>
			</div>
		<?php
		}
	}
	
	function new_column( $data ) {
		extract( $data );
		$column_name = mysql_real_escape_string( trim( $column_name ) );
		$column_desc = mysql_real_escape_string( trim( $column_desc ) );
		$date = date('Y-m-d H:i:s');
		$topic_icon = "default.jpg";
		$sql = "
			INSERT INTO 
				topic(topic_title, topic_desc, topic_create_date, user_id, topic_icon)
			VALUES
				('{$column_name}', '{$column_desc}', '{$date}', '{$creator}', '{$topic_icon}')
		";
		
		if( $this->db->query( $sql ) ) {
			return true;
		}
		return false;
	}
	
	function html_column_pic_editor( $uid, $cid ) {
		$tmp = $this->get_column( $cid );
		$column = $tmp['row'];
		extract( $column );
		$column_pic = CVIEW_PATH.$topic_icon;
		
		$attrs = array(
			'method' =>'post', 
			'class'=>"form-inline",
			'enctype'=>"multipart/form-data" 
		);
		$act =base_url().'column/chkupload';
	?>
		<div class="peditor-wrap">
			<div class="peditor-box">
				<h3><?=$topic_title?>&nbsp;专栏的当前配图:</h3>
				<div>
					<img src="<?=$column_pic?>" width="200px" height="200px"/>
				</div>
			</div>
			
			<div class="upload-box">
				<?=form_open($act, $attrs); ?>
			  <div class="form-group">
				<label class="sr-only" for="photo">修改头像</label>
				<input type="file" name="photo" size="30" />
			  </div>
				<input type="submit" class="btn btn-primary btn-sm" name="upload_btn" value="上传" placeholder="Enter email"/>
			
			</form>
			</div>
		</div>
	<?php
	}
	
	function html_edit_column($type, $uid) {
		// 修改专栏（创建、修改）
		if( $type == 0 ) {
			$form_id = "new-column-form";
			$act = base_url()."user/chknewc";
		} else if( $type == 1 ) {
			$form_id = "edit-column-form";
			$act = base_url()."user/chkeditc";
		}
	?>
		<div class="edit-column-wrap">
			<div class="edit-column-box">		
				<form role="form" id="<?=$form_id?>" method="post" action="<?=$act?>">
					<div class="form-group">
						<div id="error-place">
							<?php echo validation_errors(); ?>
						</div>	
					</div>
					<div class="form-group">
						<label for="column_name">专栏名</label>
						<input type="text" class="form-control input-sm" id="column_name" name="column_name">
					</div>
					<div class="form-group">
						<label for="column_desc">专栏介绍</label>
						<textarea class="form-control" name="column_desc" id="column_desc" rows="5"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm" name="btn_create_column">创建专栏</button>
					</div>
				</form>
			</div>
		</div>
	<?php
	}
	
	function chkupload() {
		$this->load->library('form_validation');
		$this->load->helper(array('date','url','form'));
		$this->load->model('Public_model');
		  
		$config['upload_path'] = UPLOAD_PATH. 'topic/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$p= $this->input->post();
		$f = $_FILES['photo'];
		// $f = $this->upload->data();
		$e = $this->Public_model->validate_img( $f, 2 );
		  
		$uid = '10000';
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$login = $this->User_model->user_profile($data['uid']);
			$data['login'] = $login['row'];
		}
		$data['p'] = $p;
		$data['f'] = $f;
		$data['error'] = $e;
		$this->load->view('web/user/upload_success', $data);
	}
	
	function get_newly_column_id( $uid ) {
		$sql = "
			SELECT	MAX( topic_id ) AS id
			FROM	topic
			WHERE user_id = {$uid}
		";
		$query = $this->db->query( $sql );
		return $query->row()->id;
	}
	
	
	
}
?>
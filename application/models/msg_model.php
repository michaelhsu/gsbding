<?php
class Msg_model extends CI_Model {
	var $uid = "";

	public function __construct( $id ) {
		$this->uid = $id;
	}
	
	public function set_uid( $id ) {
		$this->uid = $id;
	}
	
	public function get_id() {
		return $this->uid;
	}
	
	public function is_empty_uid() {
		return empty( $this->uid );
	}
}
?>
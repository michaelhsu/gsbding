<?php
class Admin_model extends CI_Model {

	function __construct() {	
        parent::__construct();
    }
	
	function post_story( $act, $data = array() ) {
		// 发表故事
		// $act: 0 = 新建
		//			1 = 修改
		// $data = 数组，故事信息
		if( $act == 0 ) {
			return $this->post_new_story( $data );
		} else if( $act == 1 ) {
			return $this->update_saved_story( $data );
		} 
	}
	
	function post_new_story( $data = array() ) {
		$s['user_id'] = mysql_real_escape_string( $data['author_id'] );
		$s['story_title'] = mysql_real_escape_string( trim($data[ 'story_title' ]) );
		$s['story_content'] = mysql_real_escape_string( $data[ 'story_content' ] );
		$s['topic_id'] = mysql_real_escape_string( $data['column_id'] );
		$s['post_date'] = date( 'Y-m-d H:i:s' );
		$s['recent_edit_date'] = $s['post_date'];
		$s['story_type'] = 0;
		extract( $s );
		
		$sql = "
			INSERT INTO story( story_title, story_content, story_type, topic_id,  user_id, post_date, recent_edit_date )
				VALUES('{$story_title}', '{$story_content}', '{$story_type}', '{$topic_id}', '{$user_id}', '{$post_date}', '{$recent_edit_date}')
		";
		$this->db->query( $sql );
	}
	
	function update_saved_story( $data = array() ) {
		$s['user_id'] = mysql_real_escape_string( $data['author_id'] );
		$s['story_id'] = mysql_real_escape_string( $data[ 'story_id' ] );
		$s['topic_id'] = mysql_real_escape_string( $data[ 'column_id' ] );
		$s['story_title'] = mysql_real_escape_string( trim($data[ 'story_title' ]) );
		$s['story_content'] =  mysql_real_escape_string( $data[ 'story_content' ] );
		$s['recent_edit_date'] =  date( 'Y-m-d H:i:s' );
		extract($s);
		
		if( isset( $data['btn_edit_done'] ) ) {
			// 完成修改按钮
			$sql = "
				UPDATE story
				SET story_title = '{$story_title}',
					story_content = '{$story_content}',
					recent_edit_date = '{$recent_edit_date}',
					topic_id = '{$topic_id}',
					user_id = '{$user_id}',
					story_type = '0'
				WHERE story_id = '{$story_id}'
			";
			$this->db->query( $sql );
		} 
	}
	
	function html_editor( $act=0, $story_id = -1 ) {
		if( $act == 0 ) {	// 如果是新建
			$data['story_title'] = '';
			$data['story_content'] = '';
			$data['topic_id']  = -1;
			$data['story_id']  = -1;
			$data['story_type']  = -1;
			$data['user_id'] = -1;
			$this->output_editor( $act, $data );	// 输出框
		} else if( $act == 1 ) {
			if( $story_id == -1 ) redirect();	// tbe 跳转
			
			$s = $this->get_story( $story_id );
			$this->output_editor( $act, $s );	// 输出
		}
	}
	
	function get_story( $story_id ) {
		$sql = "
			SELECT *
			FROM story 
			WHERE story_id = '{$story_id}'
		";
		$query = $this->db->query( $sql );
		return $query->row_array();
	}
	
	function output_editor(  $act, $data = array() ) {
		// 打印故事的信息
		// $data=故事标题、内容等等
		extract( $data );
	?>
		<div class="editor-wrap">
			<div class="editor-box">
					<div class="editor">
						<?php 
						$attrs = array('class' => 'form-horizontal', 'role' => 'myform', 'id'=>'write-form');
						echo form_open(base_url().'cff3fe57a9f0d38eb1ba9a70826b13ea/edit_story?act='.$act, $attrs); 
						?>
					
						<div class="form-group">
							<div id="error-place">
							<?php echo validation_errors(); ?>
							</div>
						</div>
						
						<div class="form-group">
							<input type="text" autocomplete="off" name="story_title" class="form-control title" placeholder="标题" value="<?=trim(set_value('story_title')) =="" ? $story_title : set_value('story_title');?>">
						</div>
						
						<div class="form-group">
							<textarea name="story_content" class="form-control" rows="3"><?=trim(set_value('story_content')) == "" ? $story_content : set_value('story_content') ?></textarea>
						</div>
						
						<div class="form-group">
							  <label for="column_from" class="col-sm-2 control-label">专栏</label>
							   <div class="col-sm-6">
									<?php
									$this->html_all_columns( $act, $topic_id );
									?>
								</div>
						</div>
						
						<div class="form-group">
							  <label for="column_from" class="col-sm-2 control-label">作者</label>
							   <div class="col-sm-6">
									<?php
									$this->html_all_authors( $act, $user_id);
									?>
								</div>
						</div>
						<?php
						if( $act == 0 ) {
						?>
							<button type="submit" class="btn btn-success" name="btn_post">发表</button>
						 <?php
						 } else {
						 ?>
							<button type="submit" class="btn btn-success" name="btn_edit_done">完成修改</button>
						 <?php
						 }
						 ?>
						<?php
						if( $act == 1 ) {
						?>
						<div class="attach-msg">
							<input type="hidden" name="story_id" value="<?=$story_id?>"/>
						</div>
						<?php
						}
						?>
					</form>
				</div>
			</div>
		</div>
	<?php
	}
	
	function html_all_authors( $act, $author_id = -1 ) {
		$authors_set = $this->get_all_authors();
		$selected_author = array();
		if( $act == 1 ) {
			$selected_author = $this->get_selected_author( $author_id );
		} else if( $act == 0 ) {
			$selected_author = array_shift( $authors_set );
		}
	?>
		<select name="author_id" class="form-control">
			<option value="<?=$selected_author['id']?>"><?=$selected_author['title']?></option>
			<?php
			foreach( $authors_set as $author ) {
				extract( $author );
				if( $author['id'] != $selected_author['id'] ) {
				?>
					<option value="<?=$id?>"><?=$title?></option>
				<?php
				}
			}
			?>
		</select>
	<?php
	}
	
	function get_all_authors() {
		$sql = "
			SELECT 
				user_id AS id,
				user_nickname AS title
			FROM user
			ORDER BY user_reg_date DESC
		";
		$query = $this->db->query( $sql );
		return $query->result_array(); 
	}
	
	function get_selected_author( $author_id ) {
		$sql = "
			SELECT 
				user_id AS id,
				user_nickname AS title
			FROM user
			WHERE user_id = '{$author_id}'
		";
		$query = $this->db->query( $sql );
		$row = $query->row();
		$data['id'] = $row->id;
		$data['title'] = $row->title;
		return $data;
	}
	
	
	
	function html_all_columns( $act, $column_id ) {
		$columns_set = $this->get_all_columns();
		$selected_column = array();
		if( $act == 1 ) {
			$selected_column = $this->get_selected_column( $column_id );
		} else if( $act == 0 ) {
			$selected_column = array_shift( $columns_set );
		}
	?>
		<select name="column_id" class="form-control">
			<option value="<?=$selected_column['id']?>"><?=$selected_column['title']?></option>
			<?php
			foreach( $columns_set as $column ) {
				extract( $column );
				if( $column['id'] != $selected_column['id'] ) {
				?>
					<option value="<?=$id?>"><?=$title?></option>
				<?php
				}
			}
			?>
		</select>
	<?php
	}
	
	function get_selected_column( $column_id ) {
		$sql = "
			SELECT 
				topic_id AS id,
				topic_title AS title
			FROM topic
			WHERE topic_id = '{$column_id}'
		";
		$query = $this->db->query( $sql );
		$row = $query->row();
		$data['id'] = $row->id;
		$data['title'] = $row->title;
		return $data;
	}
	
	function get_all_columns() {
		$sql = "
			SELECT 
				topic_id AS id,
				topic_title AS title
			FROM topic
			ORDER BY topic_create_date DESC
		";
		$query = $this->db->query( $sql );
		return $query->result_array(); 
	}
	
	function html_stories( $type, $page ) {
		$stories_set = $this->get_stories( $type, $page );
		// print_arr( $stories_set );
	?>
		<div class="stories-wrap">
			<div class="stories-box" id="bding-feed-list">
				<?php
				if( count( $stories_set ) == 0 ) {
					echo "<p>没有相关故事。</p>";
				} else {
					foreach( $stories_set as $item ) {
						$this->html_each_story( $item );
					}
				}
				?>
			</div>
		</div>
	<?php
	}
	
	function get_stories( $type, $page ) {
		$size = 100;
		$start = ( $page - 1 ) * $size;
		
		if( $type == 0 ) {
			$marked_date = date('Y-m-d');
			$marked_date .= " 00:00:00";
		} else if( $type == 1 ) {
			$marked_date = "0";
		}
		
		$sql = "
			SELECT 
				topic.topic_id AS cat_id,
				topic.topic_title AS cat_name,
				topic.topic_icon AS img,
				user.user_id,
				user.user_nickname,
				story.story_id,
				story.story_title,
				story.story_content,
				story.post_date,
				story.view_count
			FROM story,topic,user
			WHERE story.user_id = user.user_id
				AND story.topic_id = topic.topic_id
				AND story.post_date >= '{$marked_date}'
				AND story.story_type = '0'
			ORDER BY story.post_date DESC
			LIMIT {$start}, {$size}
		";
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function html_each_story($item ) {	// tbe
			// 显示每条的订阅信息
			extract( $item );
			$img = base_url().'comm/upload/topic/'.$img; 
			$post_date = substr( $post_date, 0, 16 );
			$story_content = $this->Public_model->story_summary( trim( strip_tags(stripslashes($story_content) ) ), 140 );
			$expand_link = "<a href='javascript:void(0)' action='fetch-story' rel='{$story_id}' class='expand-bding-link'>展开全文...</a>";
			$story_content .= $expand_link;
			
			$column_link = HOSTURL."column?id={$cat_id}"; 
			$author_link = HOSTURL."user?id={$user_id}";
			$story_link = HOSTURL."home/reddit?id={$story_id}";
			$edit_link = HOSTURL."cff3fe57a9f0d38eb1ba9a70826b13ea/edit_story?act=1&id={$story_id}";	
		?>
			<div class="bding-item" id="story-<?=$story_id?>">
				<div class="bding-title">
					<div class="bding-cat">
						<a href="<?=$column_link?>" target="_blank"><img src="<?=$img?>" width="50" height="50"/> </a>
					</div>
				
					<div class="bding-desc">
						<div class="cat-name-date">
							<div>
								<h3><a href="<?=$column_link?>" target="_blank"><?=$cat_name?></a></h3>
								<span><?=$post_date?></span>
							</div>
							
						</div>
					</div>
				</div>
					
				<div class="bding-content">
					<div>
						<h3><a href="<?=$story_link?>" action="eat-bding" rel="<?=$story_id?>" target="_blank"><?=$story_title?></a></h3>
						<h5><a href="<?=$author_link?>"  target="_blank"><i class="glyphicon glyphicon-user" style="margin-right: 5px;"></i><?=$user_nickname?></a></h5>
						<p><?=$story_content?></p>
					</div>
				</div>
				<div class="bding-interact">
					<ul class="nav nav-tabs">
						<li><a href="<?=$edit_link?>" target="_blank">编辑</a></li>
						<li><a href="javascript:void(0);" action="fake-del-bding" rel="<?=$story_id?>">删除</a></li>
						<li><a href="javascript:void(0);">阅读(<?=$view_count?>)</a></li>
					</ul>
				</div>
					<?php
					// $this->Story_model->html_story_interact( $story_id, $uid, $user_id );
					?>
				</div>
			
		<?php
		}
		
		function fake_del_bding( $story_id ) {
			$sql = "
				DELETE FROM story
				WHERE story_id = '{$story_id}'
			";
			if( $this->db->query( $sql ) ) {
				return 1;
			} 
			return 0;
		}
	
	
}
?>
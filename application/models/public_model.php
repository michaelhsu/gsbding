<?php
class Public_model extends CI_Model {


	function html_bding_feed_list($bding_list=array(), $uid, $type = 0, $s_start = 0, $s_size = 30, $c_start = 0, $c_size = 10) {
		if( $type == 0 ) {
			foreach( $bding_list as $bding ) {
				$this->html_bding_item_column( $uid, $bding, $c_start, $c_size );
			}
		} else {
			foreach( $bding_list as $bding ) {
				$this->html_bding_item_author( $uid, $bding, $c_start, $c_size );
			}
		}
			
			// 输出列表之后再绑定事件
		?>
			<script src="<?=base_url().'comm/js/user-action.js'?>"></script>
			<script>
			var voteCommentBtns = $("a[action='vote-comment']");
			$.each( voteCommentBtns, function(idx, elem) {
				$(elem).click( function() {
					var commentId = $(elem).attr("rel");
					var voted = $(elem).attr("voted");
					alert( voted );
					$.ajax({
						url: "<?=base_url()?>ajax/vote_comment?comment_id="+commentId+"&voted="+voted,
						dataType: 'json',
						success: function( data ) {
							if( data.status == 1 ) {
								if( voted == 1 ) {
									$(elem).removeClass("voted");
									$(elem).attr( 'voted', 0 );
									$("#comment-vote-"+commentId).text( data.vote_num );
								} else if( voted == 0 ) {
									$(elem).addClass("voted");
									$(elem).attr( 'voted', 1 );
									$("#comment-vote-"+commentId).text( data.vote_num );
								}
							}
						}
					});
				} );
			});
		
		var replyCommentBtns = $("a[action='reply-comment']");
		$.each( replyCommentBtns, function(idx, elem) {
			$(elem).click( function() {
				var commentId = $(elem).attr("rel");
				var name = $("#comment-"+commentId+ " .name > a").text();
				var comment_content = $("#comment-"+commentId).find(".say-words").text();
				alert( comment_content );
				comment_content = '//' + name+':'+comment_content.trim()
				$("#comment-field-"+"<?=$story_id?>"+ " textarea").val("");
				$("#comment-field-"+"<?=$story_id?>"+ " textarea").val( comment_content.trim() );
			} );
			
		});
		</script>
		<?php
	}
	
	function fetch_feed_stories( $page, $uid, $mod ) {	// fetch feed story
		// 从feeds表获取更新的布丁信息
		// return 布丁信息数组
		// limit $start, $size
		
		$size = HOME_PAGE_SIZE;
		$start = ( $page - 1 ) * $size; 
		
		if( $mod == 0 ) {	// 专栏模式
			if( $uid == -1 ) {
				$sql = "
					SELECT 
						topic.topic_id AS cat_id,
						topic.topic_title AS cat_name,
						topic.topic_icon AS img,
						user.user_id,
						user.user_nickname,
						story.story_id,
						story.story_title,
						story.story_content,
						story.post_date,
						story.view_count
					FROM story,topic,user
					WHERE story.user_id = user.user_id
						AND story.topic_id = topic.topic_id
						AND story.story_type = '0'
					ORDER BY story.post_date DESC, story.view_count DESC
					LIMIT {$start}, {$size}
				";
			} else {
				$sql = "
					SELECT 
						topic.topic_id AS cat_id,
						topic.topic_title AS cat_name,
						topic.topic_icon AS img,
						user.user_id,
						user.user_nickname,
						story.story_id,
						story.story_title,
						story.story_content,
						story.post_date,
						story.view_count
					FROM story,feeds,topic,user
					WHERE feeds.user_id = '{$uid}' 
					  AND feeds.type='{$mod}' 
					  AND story.story_id=feeds.story_id 
					  AND story.topic_id=topic.topic_id
					  AND story.user_id = user.user_id
					  AND story.story_type = '0'
					ORDER BY story.post_date DESC
					LIMIT {$start}, {$size}
				";
			
			}
		} else if( $mod == 1 ) {	// 作者模式
			if( $uid == -1 ) {
				$sql = "
					SELECT 
						user.user_id AS cat_id,
						user.user_nickname AS cat_name,
						topic.topic_id,
						topic.topic_title,
						user.user_photo AS img,
						story.story_id,
						story.story_title,
						story.story_content,
						story.post_date,
						story.view_count,
						story.user_id
					FROM story, topic,user
					WHERE story.user_id = user.user_id
						AND story.topic_id = topic.topic_id
						AND story.story_type = '0'
					ORDER BY story.view_count DESC, story.post_date DESC
					LIMIT {$start}, {$size}
				";
			}	else {
				$sql = "
					SELECT 
						user.user_id AS cat_id,
						user.user_nickname AS cat_name,
						topic.topic_id,
						topic.topic_title,
						user.user_photo AS img,
						story.story_id,
						story.story_title,
						story.story_content,
						story.post_date,
						story.view_count,
						story.user_id
					FROM story,feeds,topic,user
					WHERE feeds.user_id = '{$uid}' 
					  AND feeds.type='{$mod}' 
					  AND story.story_id=feeds.story_id 
					  AND story.topic_id=topic.topic_id
					  AND story.user_id = user.user_id
					  AND story.story_type = '0'
					ORDER BY story.post_date DESC
					LIMIT {$start}, {$size}
				";
			}
		}
		
		$query = $this->db->query( $sql );
		return $query->result_array(); 
	}
	
	function get_feed_stories_num( $uid, $mod ) {
		if( $uid == -1 ) {
			$sql = "
				SELECT 
					COUNT(story_id) AS num
				FROM story
				WHERE story.story_type = '0';
			";
		} else if( $uid >= 10000 ) {
			$sql = "
				SELECT COUNT(feed_id) AS num
				FROM feeds
				WHERE user_id = '{$uid}'
				AND type = '{$mod}'
			";
		}
		
		$query = $this->db->query( $sql );
		return $query->row()->num;
	}
	
	function html_feed_stories_list($page, $uid, $mod, $dis_type) {	// tbe
		// page 页码
		// uid 用户的id
		// type:  0=摘要显示 1=列表显示
		$feed_stories_num = $this->get_feed_stories_num( $uid, $mod );
		
		if( $feed_stories_num > 0 ) {
			$feed_stories_num = $feed_stories_num > HOME_MAX_ITEM ? HOME_MAX_ITEM : $feed_stories_num;
			$total_page = ceil( $feed_stories_num / HOME_PAGE_SIZE );
			$data = $this->fetch_feed_stories($page, $uid, $mod);
		}
		?>
		<div class="stories-wrap" >
			
			<?php
			if( $feed_stories_num > 0 ) {
				
			?>
				<div class="display-switcher">
					<a href="javascript:void(0);" action="change-homep" mod="<?=$mod?>" p="<?=$page?>" type="0"><span class="glyphicon glyphicon-th-large" title="摘要显示"></span></a>
					<a href="javascript:void(0);" action="change-homep" mod="<?=$mod?>" p="<?=$page?>" type="1"><span class="glyphicon glyphicon-th-list" title="列表显示"></span></a>
				</div>
			<?php
			}
			?>
			<div class="stories-box" id="bding-feed-list">
					<?php 
					if( $feed_stories_num == 0 ) {
						echo "<p>没有可显示的故事!</p>";
					} else {
						if( $mod == 0 ) {	// 专栏模式
							if( $dis_type == 0 ) {	
							// 摘要显示
								foreach( $data as $item ) {
									$this->html_bding_item_column($uid, $item );
								}
							} else if( $dis_type == 1 ) {							
							// 列表显示
					?>
								<div class='column-bdings-box-hoz'>
									<ul>
								<?php
								foreach( $data as $item ) {
									$this->User_model->html_user_story_item_hoz($uid, $item );
								}
								?>
									</ul>
								</div>
						<?php
							}
						} else if( $mod == 1 ) {	// 作者模式
							if( $dis_type == 0 ) {	
							// 摘要显示
								foreach( $data as $item ) {
									$this->html_bding_item_author($uid, $item );
								}
							} else if( $dis_type == 1 ) {	
							// 列表显示
						?>
								<div class='column-bdings-box-hoz'>
									<ul>
								<?php
								foreach( $data as $item ) {
									$this->Column_model->html_column_story_item_hoz($uid, $item );
								}
								?>
									</ul>
								</div>
						<?php
							}
						}
						?>
					
					<?php
					}
					?>
					<script src="<?=base_url().'comm/js/user-action.js'?>?time=<?=time()?>"></script>
			</div>
		</div>
		<?php
		if( @$total_page > 1 ) {
		?>
			<div class="page-switcher">
				<?php
					if( $page > 1 ) {
				?>
						<span><a href="javascript:void(0)" action="change-homep" type="<?=$dis_type?>" id="prev-page" p="<?=$page - 1?>" mod="<?=$mod?>">上一页</a></span>
				<?php
					}
					echo "<select id='sel-home-page' mod='{$mod}' type='{$dis_type}'>";
					for($i = 1; $i <= $total_page; $i++) {
				?>
						<option value="<?=$i?>" <?php if($page == $i) echo "selected";?>>第<?=$i?>页</option>
				
				<?php
					}
					echo "</select>";
					if( $page < $total_page ) {
				?>
						<span><a href="javascript:void(0)" action="change-homep" type="<?=$dis_type?>" id="prev-page" p="<?=$page +1?>" mod="<?=$mod?>">下一页</a></span>
				<?php
					}
				?>
			</div>
		
		<?php
		}
		?>
			<?php
	}
	
	
	function html_bding_item_column($uid, $item, $c_start=0, $c_size=100 ) {	// tbe
			// 显示每条的订阅信息
			extract( $item );
			$img = base_url().'comm/upload/topic/'.$img; 
			$post_date = substr( $post_date, 0, 16 );
			$story_content = $this->story_summary( trim( strip_tags(stripslashes($story_content) ) ), 140 );
			$expand_link = "<a href='javascript:void(0)' action='fetch-story' rel='{$story_id}' class='expand-bding-link'>展开全文...</a>";
			$story_content .= $expand_link;
			
			$c_link = HOSTURL.'column?id='.$cat_id;
			$u_link = HOSTURL.'user?id='.$user_id;
			$s_link = HOSTURL.'home/reddit?id='.$story_id;
			
		?>
			<div class="bding-item" id="story-<?=$story_id?>">
				<div class="bding-title">
					<div class="bding-cat">
						<a href="<?=$c_link?>" target="_blank"><img src="<?=$img?>" width="50" height="50" class="img-rounded"/> </a>
					</div>
				
					<div class="bding-desc">
						<div class="cat-name-date">
							<div>
								<h3><a href="<?=$c_link?>" target="_blank"><?=$cat_name?></a></h3>
								<span><?=$post_date?></span>
							</div>
							
						</div>
					</div>
				</div>
					
					<div class="bding-content">
						<div>
							<h3><a href="<?=$s_link?>" action="eat-bding" rel="<?=$story_id?>" target="_blank"><?=$story_title?></a></h3>
							<h5><a href="<?=$u_link?>"  target="_blank"><i class="glyphicon glyphicon-user" style="margin-right: 5px;"></i><?=$user_nickname?></a></h5>
							<p><?=$story_content?></p>
						</div>
					</div>
					<?php
					$this->Story_model->html_story_interact( $story_id, $uid, $user_id );
					?>
				</div>
			
			
		<?php
		}
		
		function html_bding_item_author($uid, $item, $c_start=0, $c_size=100 ) {	// tbe
			// 显示每条的订阅信息
			extract( $item );
			// print_arr( $item );	// tbd
			$img = UVIEW_PATH.$img; 
			$post_date = substr( $post_date, 0, 16 );
			$story_content = $this->story_summary( trim( strip_tags(stripslashes($story_content) ) ), 140 );
			$expand_link = "<a href='javascript:void(0)' action='fetch-story' rel='{$story_id}' class='expand-bding-link'>展开全文...</a>";
			$story_content .= $expand_link;
			
			$c_link = HOSTURL.'column?id='.$topic_id;
			$u_link = HOSTURL.'user?id='.$cat_id;
			$s_link = HOSTURL.'home/reddit?id='.$story_id;
			
		?>
			<div class="bding-item" id="story-<?=$story_id?>">
				
				<div class="bding-title">
					<div class="bding-cat">
						<a href="<?=$u_link?>" target="_blank"><img src="<?=$img?>" width="50" height="50"class="img-rounded"/> </a>
					</div>
					
					<div class="bding-desc">
						<div class="cat-name-date">
							<div>
								<h3><a href="<?=$u_link?>" target="_blank"><?=$cat_name?></a></h3>
								<span><?=$post_date?></span>
							</div>
							
						</div>
					</div>
				</div>
		
					
					<div class="bding-content">
						<div>
							<h3><a href="<?=$s_link?>" action="eat-bding" rel="<?=$story_id?>" target="_blank"><?=$story_title?></a></h3>
							<h5><a href="<?=$c_link?>"  target="_blank"><i class="glyphicon glyphicon-paperclip" style="margin-right: 5px;"></i><?=$topic_title?></a></h5>
							<p><?=$story_content?></p>
						</div>
					</div>
					<?php
					$this->Story_model->html_story_interact( $story_id, $uid, $user_id );
					?>
					
				</div>
			
		<?php
		}
		
		// 60
	function story_summary( $story_content, $size ) {
		$story_content = strip_tags( $story_content );
		$story_content = $this->cut_str( $story_content, $size );
		return $story_content;
	}
	
	function content_summary( $content, $size ) {
		$content = strip_tags( $content );
		$content = $this->cut_str( $content, $size );
		return $content;
	}
	
	// 61
	function cut_str($sourcestr,$cutlength) { 
		$returnstr=''; 
		$i=0; 
		$n=0; 
		$str_length=strlen($sourcestr);//字符串的字节数 
		while (($n<$cutlength) and ($i<=$str_length)) 
		{ 
			$temp_str=substr($sourcestr,$i,1); 
			$ascnum=Ord($temp_str);//得到字符串中第$i位字符的ascii码 
			if ($ascnum>=224) //如果ASCII位高与224，
			{ 
				$returnstr=$returnstr.substr($sourcestr,$i,3); //根据UTF-8编码规范，将3个连续的字符计为单个字符 
				$i=$i+3; //实际Byte计为3
				$n++; //字串长度计1
			}
			elseif ($ascnum>=192) //如果ASCII位高与192，
			{ 
				$returnstr=$returnstr.substr($sourcestr,$i,2); //根据UTF-8编码规范，将2个连续的字符计为单个字符 
				$i=$i+2; //实际Byte计为2
				$n++; //字串长度计1
			}
			elseif ($ascnum>=65 && $ascnum<=90) //如果是大写字母，
			{ 
				$returnstr=$returnstr.substr($sourcestr,$i,1); 
				$i=$i+1; //实际的Byte数仍计1个
				$n++; //但考虑整体美观，大写字母计成一个高位字符
			}
			else //其他情况下，包括小写字母和半角标点符号，
			{ 
				$returnstr=$returnstr.substr($sourcestr,$i,1); 
				$i=$i+1; //实际的Byte数计1个
				$n=$n+0.5; //小写字母和半角标点等与半个高位字符宽...
			} 
		} 
		
		if ($str_length>$cutlength){
			$returnstr = $returnstr ;//超过长度时在尾处加上省略号
		}
		return $returnstr;
	}
	
	function validate_img( $data=array(), $max_size ) {
		// 验证图片的有效性
		// return error = 7 or 8
		extract( $data );
		$allowed_types = array(
			'image/pjpeg',
			'image/jpeg',
			'image/jpg',
			'image/png',
			'image/x-png',
			'image/gif'
		);
		if( !in_array( $type, $allowed_types ) ) {
			return 7;
		}
		if( $size > $max_size*1048576 ) {
			return 8;
		}
		return 0;
	}
	
	public function display_cut_img_box($data, $max_width=560, $preview_width, $upload_type=0) {
		// 显示待裁剪的图片
		// type 0=用户头像 1=专栏配图
		extract( $data );
		if( $upload_type == 0 ) {
			$upload_dir = UPLOAD_PATH.'user/';
			$view_dir = UVIEW_PATH;
			$act = base_url().'user/cut';
		} else if( $upload_type == 1 ) {
			$upload_dir = UPLOAD_PATH.'topic/';
			$view_dir = CVIEW_PATH;
			$act = base_url().'column/cut';
		}
		
		$ext = strtolower( substr( $name, strrpos( $name, '.') + 1 ) ) ;
		if( $upload_type == 0 ) {
			$photo_name = 'author_'.$this->session->userdata('uid').".".$ext;
		} else if( $upload_type == 1 ) {
			$photo_name = 'column_'.$this->session->userdata('cid').".".$ext;
		}
		// $photo_name = "photo_".time().".".$ext;	
		$save_path = $upload_dir.$photo_name;
		$view_path = $view_dir.$photo_name;
		$this->session->set_userdata('save_path', $save_path);
		
		move_uploaded_file( $tmp_name, $save_path );	// 先将文件保存在服务器上
		$i = $this->img_size( $save_path );
		$i['path'] = $save_path;
		
		if( $i['width'] > $max_width ) {
			$scale = $max_width / $i['width'];
			$this->resize_img( $i, $scale );
			$i['width'] = $max_width;
			$i['height'] = $scale*$i['height'];
		}
		?>
		<div class="cut-photo-wrap">
			<div class="cut-photo-box">
				<p>左边大图中选择裁剪区域，然后单击“裁剪”按钮即可完成裁剪</p><br/>
				<div class="big-img">
					<img src="<?=$view_path?>"  id="thumbnail" title="待截图像" />
				</div>
				
				<div  class="small-img" id="small-img" style="width:<?=$preview_width?>px; height:<?=$preview_width?>px;">
					<img src="<?=$view_path?>" alt="preview" />
				</div>
				
				<br style="clear:both;"/>
					<?php
					$attr=array(
						'class'=>'inline',
						'role'=>'form'
					);
					echo form_open( $act, $attr );
					?>
					<input type="hidden" name="x1" value="" id="x1" />
					<input type="hidden" name="y1" value="" id="y1" />
					<input type="hidden" name="x2" value="" id="x2" />
					<input type="hidden" name="y2" value="" id="y2" />
					<input type="hidden" name="w" value="" id="w" />
					<input type="hidden" name="h" value="" id="h" />
					<br/>
					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-sm" name="cut_photo_btn" value="裁剪" id="save_thumb" style="margin-right: 20px;" />
						<a href="javascript:void(0);" class="btn btn-primary btn-sm" >重新上传</a>
					</div>
				</form>
			</div>
		</div>
		
		<script>
			function preview(img, selection) { 
				var scaleX = <?=$preview_width;?> / selection.width; 
				var scaleY = <?=$preview_width;?> / selection.height; 
				
				$('#small-img > img').css({ 
					width: Math.round(scaleX * <?=$i['width'];?>) + 'px', 
					height: Math.round(scaleY * <?=$i['height'];?>) + 'px',
					marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
					marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
				});

				$('#x1').val(selection.x1);
				$('#y1').val(selection.y1);
				$('#x2').val(selection.x2);
				$('#y2').val(selection.y2);
				$('#w').val(selection.width);
				$('#h').val(selection.height);
			} 

			$(document).ready(function () { 
				$('#save_thumb').click(function() {
					var x1 = $('#x1').val();
					var y1 = $('#y1').val();
					var x2 = $('#x2').val();
					var y2 = $('#y2').val();
					var w = $('#w').val();
					var h = $('#h').val();
					if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
						alert("You must make a selection first");
						return false;
					}else{
						return true;
					}
				});
			}); 

			$(window).load(function () { 
				$('#thumbnail').imgAreaSelect({ aspectRatio: '1:1', onSelectChange: preview }); 
			});
			</script>
		
		<?php
		
	}
	
	public function img_size( $img ) {
		$s = getimagesize($img);
		$data['width'] = $s[0];
		$data['height'] = $s[1];
		$data['mime_type'] = image_type_to_mime_type($s[2]);
		return $data;
	}
	
	function resize_img($data=array(), $scale) {
		extract( $data );
		$new_width = ceil($width * $scale);
		$new_height = ceil($height * $scale);
		$new_img = imagecreatetruecolor($new_width,$new_height);
		switch($mime_type) {
			case "image/gif":
				$source=imagecreatefromgif($path); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($path); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($path); 
				break;
		}
		
		imagecopyresampled($new_img,$source,0,0,0,0,$new_width,$new_height,$width,$height);
		
		switch($mime_type) {
			case "image/gif":
				imagegif($new_img,$path); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($new_img,$path,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($new_img,$path);  
				break;
		}
		
		chmod($path, 0777);
		return $path;
	}
	
	
	
	public function crop_img( $data=array(), $upload_type = 0 ) {
		extract( $data );
		$x1 = isset( $_POST["x1"] ) ? $_POST["x1"]: 0;
		$y1 = isset( $_POST["y1"] ) ? $_POST["y1"]: 0;
		$x2 = isset( $_POST["x2"] ) ? $_POST["x2"]: $sw;
		$y2 = isset( $_POST["y2"] ) ? $_POST["y2"] : $sw;
		$w = isset( $_POST["w"] ) ? $_POST['w'] : $sw;
		$h = isset( $_POST["h"] ) ? $_POST['h'] : $sw;
		//Scale the image to the thumb_width set above
		$scale = $sw/$w;
		$cropped = $this->resize_thumb_image($img, $img,$w,$h,$x1,$y1,$scale);
		$name = basename( $img );
		if( $upload_type == 0 ) {
			$sql = "
				UPDATE	user
				SET	user_photo = '{$name}'
				WHERE user_id = '{$uid}'
			";
		} else if( $upload_type == 1 ) {
			$sql = "
				UPDATE	topic
				SET	topic_icon = '{$name}'
				WHERE topic_id = '{$cid}'
			";
		}
		
		if( $this->db->query( $sql ) ) {
			return true;
		}
		return false;
	}
	
	function resize_thumb_image($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);  
				break;
		}
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;
	}
	
	function follow_or_not( $type, $uid, $xid ) {
		// 查看用户是否关注了专栏或作者
		// type 0=专栏 1=作者
		if( $type == 0 ) {
			$sql = "
				SELECT COUNT(*) AS flag
				FROM	follow_topic
				WHERE topic_id = '{$xid}'
					AND user_id = '{$uid}'
			";
		} else if( $type == 1 ) {
			$sql ="
				SELECT COUNT(*) AS flag
				FROM 	follow_user
				WHERE user_id = '{$uid}'
					AND follow_target_id = '{$xid}'
			";
		}
		
		$query = $this->db->query( $sql );
		return $query->row()->flag;
	}
	
	
	
	
	
	
	
	
	function html_read_history( $type, $data ) {
		extract( $data );
		if( $type == 0 ) {
			$title = "今日的阅读记录";
		} else if( $type == 1 ) {
			$title = "以往的阅读记录";
		}
		
	?>
	<div class="history-wrap">
		<div class="history-box">
		<?php
			if( count( $list ) == 0 ) {
				echo "<p>".$day."没有阅读记录</p>";
			} else {
				// echo "<h3>{$title}</h3>";
				foreach( $list as $item ) {	// 打印每一条阅读记录
					echo '<ul class="history-item">';
					$this->html_record_history_item( $type, $item );
					echo '</ul>';
				}
			}
		?>
		</div>
	</div>
	<?php
	}
	
	function html_record_history_item( $type, $data = array() ) {
		// 打印每一条阅读记录
		extract( $data );
		$bding_link = HOSTURL.'home/reddit?id='.$story_id;
		$topic_link = HOSTURL.'column?id='.$topic_id;
		$author_link = HOSTURL.'user?id='.$user_id;
		
		$s = $this->story_summary( $story_title, 20 );
		$t = $this->story_summary( $topic_title, 10 );
		if( $type == 0 ) 
			$date = substr( $record_date, 11, 5 );
		else if( $type == 1) 
			$date = substr( $record_date, 0, 10 );
	?>
		<li class="date"><?=$date?></li>
		<li class="bding"><a href="<?=$bding_link?>" action="eat-bding" rel="<?=$story_id?>" title="<?=$story_title?>" target="_blank"><?=$s?></a></li>
		<li class="column"><a href="<?=$topic_link?>" title="<?=$topic_title?>"><i class="glyphicon glyphicon-paperclip" style="margin-right: 5px;"></i><?=$t?></a></li>
		<li class="author"><a href="<?=$author_link?>" title="<?=$user_nickname?>"><i class="glyphicon glyphicon-user" style="margin-right: 5px;"></i><?=$user_nickname?></a></li>
	<?php
	}
	
	function get_top_list( $type, $count, $start, $size ) {
		$sql = "";
		$title = "";
		switch( $type ) {
			case 0:
				$sql = "
				SELECT
					topic.topic_id, 
					topic.topic_title, 
					story.story_id,  
					story.story_title, 
					story. view_count,
					story.user_id,
					user.user_nickname,
					user.user_photo
				FROM story, topic, user
				WHERE story.view_count >= {$count} 
					AND story.topic_id=topic.topic_id 
					AND user.user_id = story.user_id
					AND story.story_type='0'
				ORDER BY view_count DESC
				LIMIT {$start}, {$size} "; 
				$title = "总榜";
				break;
			case 1:
				// $date = time();
				$date = date( 'Y-m-d' );
				$date .= ' 00:00:00';
				$sql  =  "
					SELECT 
						topic.topic_id, 
						topic.topic_title, 
						story.story_id,  
						story.story_title, 
						story. view_count,
						story.user_id,
						user.user_nickname,
						user.user_photo
					FROM story, topic, user 
					WHERE story.view_count >= {$count} 
						AND story.topic_id=topic.topic_id 
						AND story.post_date >= '{$date}'
						AND user.user_id = story.user_id
						AND story.story_type='0'
					ORDER BY view_count DESC
					LIMIT {$start}, {$size} "; 
				$title = "今日";
				break;
			case 2:
				$date = time() - (7*24*60*60);
				$date = date( 'Y-m-d H:i:s' , $date );
				// echo $date;
				$sql  =  "
					SELECT 
						topic.topic_id, 
						topic.topic_title, 
						story.story_id,  
						story.story_title, 
						story. view_count,
						story.user_id,
						user.user_nickname,
						user.user_photo
					FROM story, topic, user 
					WHERE story.view_count >= {$count} 
						AND story.topic_id=topic.topic_id 
						AND story.post_date >=' {$date}'
						AND user.user_id = story.user_id
						AND story.story_type='0'
							  ORDER BY view_count DESC
					LIMIT {$start}, {$size}"; 
				$title = "本周";
				break;
			case 3:
				$date = time() - (30*24*60*60);
				$date = date( 'Y-m-d H:i:s' , $date );
				$sql  =  "
					SELECT 
						topic.topic_id, 
						topic.topic_title, 
						story.story_id,  
						story.story_title, 
						story. view_count,
						story.user_id,
						user.user_nickname,
						user.user_photo
					FROM story, topic, user 
					WHERE story.view_count >= {$count} 
						AND story.topic_id=topic.topic_id 
						AND story.post_date >= '{$date}'
						AND user.user_id = story.user_id
						AND story.story_type='0'
					ORDER BY view_count DESC
					LIMIT {$start}, {$size}"; 
				$title = "本月";
				break;
		}
		
		$query = $this->db->query( $sql );
		
		$result['title'] = $title;
		$result['story_list'] = $query->result_array();
		return $result;
	}
	
	function html_top_list_item( $key, $data = array() ) {
		// 打印每一条阅读记录
		extract( $data );
		$bding_link = HOSTURL.'home/reddit?id='.$story_id;
		$topic_link = HOSTURL.'column?id='.$topic_id;
		$author_link = HOSTURL.'user?id='.$user_id;
		
		$s = $this->story_summary( $story_title, 20 );
		$t = $this->story_summary( $topic_title, 10 );
		$key = $key + 1;
	?>
		<li class="rank-num"><?=$key?>.</li>
		<li class="bding"><a href="<?=$bding_link?>" action="eat-bding" rel="<?=$story_id?>" title="<?=$story_title?>" target="_blank"><?=$s?></a></li>
		<li class="column"><a href="<?=$topic_link?>" title="<?=$topic_title?>"><i class="glyphicon glyphicon-paperclip" style="margin-right: 5px;"></i><?=$t?></a></li>
		<li class="author"><a href="<?=$author_link?>" title="<?=$user_nickname?>"><i class="glyphicon glyphicon-user" style="margin-right: 5px;"></i><?=$user_nickname?></a></li>
		<li class="view-count"><i class="glyphicon glyphicon-eye-open" style="margin-right: 5px;"></i><?=$view_count?></li>
	<?php
	}
	
	function html_top_list( $data ) {
		extract( $data );
	?>
		<div class="rank-wrap">
				<div class="rank-box">
			<?php
				
				// echo $title;
				if( count( $story_list ) == 0 ) {
					echo "<p>".$title."榜单为空</p>";
				} else {
					foreach( $story_list as $key => $item ) {
						echo '<ul class="rank-item">';
						$this->html_top_list_item( $key, $item );
						echo '</ul>';
					}
				}
			?>
				</div>
			</div>
	<?php
	}
	
	public function get_new_comments($uid, $start, $size) {
		if( $uid == -1 ) {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					comment.comment_content AS content,
					comment.comment_date AS date,
					story.story_id, 
					story.story_title
				FROM user, story, comment
				WHERE comment.story_id = story.story_id
					AND comment. user_id = user.user_id
				ORDER BY comment_date DESC
				LIMIT {$start}, {$size}
			";
		} else {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					comment.comment_content AS content,
					comment.comment_date AS date,
					story.story_id, 
					story.story_title
				FROM user, story, comment
				WHERE comment.story_id = story.story_id
					AND comment. user_id = user.user_id
					AND story.story_id NOT IN (
						SELECT story_id 
						FROM history
						WHERE user_id = '{$uid}'
					)
				ORDER BY comment_date DESC
				LIMIT {$start}, {$size}
			";

		
		}
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	public function html_new_comments($uid, $start, $size) {	// tbe
		$data = $this->get_new_comments($uid, $start, $size);
		if( count( $data ) > 0 ) {
	?>
		<div class="attach-wrap">
				<div class="title"><span>最新评论<i style="color: #008C00">/ new comments</i></span></title>
			</div>
			
			<div class="attach-box">
				<ul>
				<?php
				foreach( $data as $item ) {
					extract( $item );
					$user_photo = UVIEW_PATH.$user_photo;
					$user_link = HOSTURL.'user/?id='.$user_id;
					$story_link = HOSTURL.'home/reddit?id='.$story_id;
					$s_nickname = $this->story_summary( $user_nickname, 4 );
					$s_title = $this->story_summary( $story_title, 7 );
					$date = substr( $date, 0, 16 );
				?>
				<li class="comment-item">
					<div class="media">
					  <a class="pull-left" href="<?=$user_link?>" target="_blank">
						<img class="media-object" src="<?=$user_photo?>" alt="<?=$user_nickname?>" width="20px" height="20px">
					  </a>
					  <div class="media-body">
						<p><a href="<?=$user_link?>" target="_blank" title="<?=$user_nickname?>"><?=$s_nickname?></a>&nbsp;评论了:&nbsp;<a href="<?=$story_link?>" target="_blank" title="<?=$story_title?>" action="eat-bding" rel="<?=$story_id?>"><?=$s_title?></a></p>
					  </div>
					</div>
					<div class="content">
						<p><?=$content?></p>
					</div>
					<div class="date">
						<p><?=$date?></p>
					</div>
				</li>
				<?php  
				} 
				?>
				</ul>
			</div>
			
		</div>
	<?php
		}
	}
	
	public function get_new_votes($uid, $start, $size) {
		if( $uid == -1 ) {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					story.story_id, 
					vote.vote_date AS date,
					story.story_title
				FROM user, story, vote
				WHERE vote.story_id = story.story_id
					AND vote. user_id = user.user_id
					AND vote.vote_type = '1'
				ORDER BY vote.vote_date DESC
				LIMIT {$start}, {$size}
			";
		} else {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					story.story_id, 
					vote.vote_date AS date,
					story.story_title
				FROM user, story, vote
				WHERE vote.story_id = story.story_id
					AND vote. user_id = user.user_id
					AND vote.vote_type = '1'
					AND story.story_id NOT IN (
						SELECT story_id 
						FROM history
						WHERE user_id = '{$uid}'
					)
				ORDER BY vote.vote_date DESC
				LIMIT {$start}, {$size}
			";
			
		}
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	public function html_new_votes($uid,$start, $size) {
		$data = $this->get_new_votes($uid, $start, $size);
		if( count( $data ) > 0 ) {
	?>
		<div class="attach-wrap">
			<div class="title"><span>最新投票<i style="color: #B38600">/ new votes</i></span></title></div>
			<div class="attach-box">
				<ul>
				<?php
				foreach( $data as $item ) {
					extract( $item );
					$user_photo = UVIEW_PATH.$user_photo;
					$user_link = HOSTURL.'user/?id='.$user_id;
					$story_link = HOSTURL.'home/reddit?id='.$story_id;
					$s_nickname = $this->story_summary( $user_nickname, 4 );
					$s_title = $this->story_summary( $story_title, 7 );
				?>
				<li class="vote-item">
					<div class="media">
					  <a class="pull-left" href="<?=$user_link?>">
						<img class="media-object" src="<?=$user_photo?>" alt="<?=$user_nickname?>" title="<?=$user_nickname?>" width="20px">
					  </a>
					  <div class="media-body">
						<p><a href="<?=$user_link?>" target="_blank" title="<?=$user_nickname?>"><?=$s_nickname?></a>&nbsp;赞了:&nbsp;<a href="<?=$story_link?>" target="_blank" title="<?=$story_title?>" action="eat-bding" rel="<?=$story_id?>"><?=$s_title?></a></p>
					  </div>
					</div>
				</li>
				<?php  
				} 
				?>
				</ul>
			</div>
		</div>
	<?php
		}
	}
	
	public function get_new_stories($uid, $start, $size) {
		if( $uid == -1 ) {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					story.story_id, 
					story.post_date AS date,
					story.story_title
				FROM user, story
				WHERE  story. user_id = user.user_id
					AND story.story_type = '0'
				ORDER BY story.post_date DESC
				LIMIT {$start}, {$size}
			";
		} else {
			$sql = "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					story.story_id, 
					story.post_date AS date,
					story.story_title
				FROM user, story
				WHERE  story. user_id = user.user_id
					AND story.story_type = '0'
					AND story.story_id NOT IN (
						SELECT story_id 
						FROM history
						WHERE user_id = '{$uid}'
					)
				ORDER BY story.post_date DESC
				LIMIT {$start}, {$size}
			";
		}
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	public function html_new_stories($uid, $start, $size) {
		$data = $this->get_new_stories($uid, $start, $size);
		if( count( $data ) > 0 ) {
	?>
		<div class="attach-wrap">
			<div class="title"><span>最新故事<i style="color: #B30000">/ new stories</i></span></title></div>
			<div class="attach-box">
				<ul>
				<?php
				foreach( $data as $item ) {
					extract( $item );
					$user_photo = UVIEW_PATH.$user_photo;
					$user_link = HOSTURL.'user/?id='.$user_id;
					$story_link = HOSTURL.'home/reddit?id='.$story_id;
					$s_nickname = $this->story_summary( $user_nickname, 4 );
					$s_title = $this->story_summary( $story_title, 7 );
				?>
				<li class="story-item">
					<div class="media">
						<a class="pull-left" href="<?=$user_link?>">
							<img class="media-object" src="<?=$user_photo?>" alt="<?=$user_nickname?>" width="20px">
						</a>
						<div class="media-body">
							<p><a href="<?=$user_link?>" target="_blank" title="<?=$user_nickname?>"><?=$s_nickname?></a>&nbsp;发表故事:&nbsp;<a href="<?=$story_link?>" target="_blank" title="<?=$story_title?>" action="eat-bding" rel="<?=$story_id?>" ><?=$s_title?></a></p>
						</div>
					</div>
				</li>
				<?php  
				} 
				?>
				</ul>
			</div>
		</div>
	<?php
		}
	}
	
	public function get_hot_stories( $uid, $count, $start, $size ) {
		// 获取一个月内的性感故事
		// $date = time() - (30*24*60*60);
		// $date = date( 'Y-m-d H:i:s' , $date );
		$date = 0;
		if( $uid == -1 ) {
				$sql  =  "
					SELECT 
						user.user_id, user.user_photo, user.user_nickname,
						story.story_id, 
						story.post_date AS date,
						story.story_title,
						story.view_count
					FROM story, user 
					WHERE story.view_count >= {$count} 
						AND story.post_date >= '{$date}',
						AND story.story_type = '0'
						AND user.user_id = story.user_id
					ORDER BY view_count DESC
					LIMIT {$start}, {$size}"; 
		} else {
			$sql  =  "
				SELECT 
					user.user_id, user.user_photo, user.user_nickname,
					story.story_id, 
					story.post_date AS date,
					story.story_title,
					story.view_count
				FROM story, user 
				WHERE story.view_count >= {$count} 
					AND story.post_date >= '{$date}'
					AND user.user_id = story.user_id
					AND story.story_type = '0'
					AND story.story_id NOT IN (
					SELECT story_id 
					FROM history
					WHERE user_id = '{$uid}'
				)
				ORDER BY view_count DESC
				LIMIT {$start}, {$size}"; 
		}
		
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	public function html_hot_stories( $uid, $count, $start, $size ) {	// 获取受捧文章
		// 获取本周受捧文章
		$data = $this->get_hot_stories($uid, $count, $start, $size);
		if( count($data) > 0 ) {
		?>
			<div class="attach-wrap">
				<div class="title"><span>性感故事<i style="color: #1A0066">/ hot stories</i></span></title></div>
				<div class="attach-box">
					<ul>
					<?php
					foreach( $data as $item ) {
						extract( $item );
						$user_photo = UVIEW_PATH.$user_photo;
						$user_link = HOSTURL.'user/?id='.$user_id;
						$story_link = HOSTURL.'home/reddit?id='.$story_id;
						$s_nickname = $this->story_summary( $user_nickname, 4 );
						$s_title = $this->story_summary( $story_title, 7 );
						$date = substr( $date, 0, 10 );
					?>
					<li class="story-item">
						<div class="media">
						  <a class="pull-left" href="#">
							<img class="media-object" src="<?=$user_photo?>" alt="<?=$user_nickname?>" width="20px">
						  </a>
						  <div class="media-body">
							<p><a href="<?=$user_link?>" target="_blank" title="<?=$user_nickname?>"><?=$s_nickname?></a>&nbsp;的作品:&nbsp;<a href="<?=$story_link?>" target="_blank" title="<?=$story_title?>" action="eat-bding" rel="<?=$story_id?>"><?=$s_title?></a></p>
							<p class="count"><span><?=$view_count?>次阅读</span>&nbsp;&nbsp;&nbsp;&nbsp;<span><?=$date?></span></p>
						  </div>
						</div>
					</li>
					<?php  
					} 
					?>
					</ul>
				</div>
		</div>
		<?
		}
	}
	
	function  html_write_link() {
		$link = HOSTURL.'home/write';
	?>
		<div class="attach-wrap" style="text-align: center;">
			<a href="<?=$link?>" class="btn btn-success btn-large">开始创作故事</a>
		</div>
	<?php
	}
	
	function html_declare( $content="" ) {
	?>
		<div class="attach-wrap">
			<div class="title"><span>声明<i style="color:#FF5C26;">/ declare</i></span></div>
			<div class="attach-box">
			<p>
				由于developer极度讨厌IE，所以建议使用Chrome > 360 > Firefox。
			</p>
			<p style="text-align:right;"><a href="http://www.google.cn/intl/zh-CN/chrome/browser/?installdataindex=chinabookmarkcontrol&brand=CHUN" target="_blank">Chrome下载</a></p>
			</div>
		</div>
	<?php
	}
	
	function html_copyright_declare( $content="" ) {
	?>
		<div class="attach-wrap copyright-declare">
			<div class="title"><span>版权说明<i style="color:#FF5C26;">/ copyrigh declare</i></span></div>
			<div class="attach-box">
			<p>
				首先，故事布丁默认故事为作者的原创，并保护作者的版权；
				其次，如非原创，请注明作者与来源；
				最后，如果您认为该故事存在版权问题，请让我们听到您的声音。</p>
			</p>
			<p style="text-align:right;"><a href="#" target="_blank">版权问题</a></p>
			</div>
		</div>
	<?php
	}
	
	function get_suggest_columns( $start, $size ) {
		$sql = "
			DROP TABLE IF EXISTS tmp_column_table
		";
		$this->db->query( $sql );
		$sql = "
			CREATE TEMPORARY  TABLE IF NOT EXISTS tmp_column_table
			SELECT DISTINCT
				story.story_id,
				topic.topic_id,
				topic.topic_icon,
				topic.topic_title
			FROM	topic, story
			WHERE 
				story.topic_id = topic.topic_id
				AND story.story_type = '0'
			ORDER BY story.story_id DESC
			LIMIT 0, 100
		";
		$this->db->query( $sql );
		
		$sql = "
			SELECT DISTINCT
				topic_id,
				topic_icon,
				topic_title
			FROM tmp_column_table
			LIMIT {$start}, {$size}
		";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function	html_suggest_columns( $start, $size ) {
		$data = $this->get_suggest_columns( $start, $size );
		if( count( $data ) > 0 ) {
		?>
			<div class="attach-wrap">
				<div class="title"><span>推荐专栏<i style="color: #008C23">/ why not try</i></span></title></div>
				<div class="attach-box">
				<ul>
				<?php
					// print_arr( $data );
					foreach( $data as $item ) {
						extract( $item );
						$column_link = HOSTURL."column/?id=".$topic_id;
						$column_photo = CVIEW_PATH.$topic_icon;
				?>
						<li class="follow-item"><a href="<?=$column_link?>" target="_blank" title="<?=$topic_title?>" alt="<?=$topic_title?>"><img src="<?=$column_photo?>" width="50px" height="50px" class="img-rounded"/></a></li>
				<?php
					}
				?>
				</ul>
				</div>
			</div>
		<?php
		}
	}
	
	function get_suggest_authors( $start, $size ) {
		$sql = "
			DROP TABLE IF EXISTS tmp_table
		";
		$this->db->query( $sql );
		
		$sql = "
			CREATE TEMPORARY TABLE  IF NOT EXISTS  tmp_table 
			SELECT DISTINCT
				story.story_id,
				user.user_id,
				user.user_photo,
				user.user_nickname
			FROM	user, story
			WHERE 
				story.user_id = user.user_id
				AND story.story_type = '0'
			ORDER BY story.story_id DESC
			LIMIT 0, 100
		";
		$this->db->query( $sql );
		$sql = "
			SELECT DISTINCT
				user_id,
				user_photo,
				user_nickname
			FROM tmp_table
		";
	
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function	html_suggest_authors( $start, $size ) {
		$data = $this->get_suggest_authors( $start, $size );
		if( count( $data ) > 0 ) {
		?>
			<div class="attach-wrap">
				<div class="title"><span>推荐作者<i style="color: #008C23">/ why not try</i></span></title></div>
				<div class="attach-box">
				<ul>
				<?php
					// print_arr( $data );
					foreach( $data as $item ) {
						extract( $item );
						$author_link = HOSTURL."user/?id=".$user_id;
						$author_photo =UVIEW_PATH.$user_photo;
				?>
						<li class="follow-item"><a href="<?=$author_link?>" target="_blank" title="<?=$user_nickname?>" alt="<?=$user_nickname?>"><img src="<?=$author_photo?>" width="50px" height="50px"  class="img-rounded"/></a></li>
				<?php
					}
				?>
				</ul>
				</div>
			</div>
		<?php
		}
	}
	
	function search_column_label( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );\
		$sql = "SELECT 
			topic_id, 
			topic_title as label 
			FROM topic 
			WHERE topic_title LIKE '%{$key}%'
		";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}

	function search_story_label( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );\
		$sql = "SELECT story_id, story_title as label FROM story WHERE story_title LIKE '%{$key}%'";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}

	function search_user_label( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );\
		$sql = "SELECT user_nickname as label FROM user WHERE user_nickname LIKE '%{$key}%'";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function search_story_result( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );
		$sql = "SELECT * FROM story WHERE story_title LIKE '%{$key}%'";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function search_user_result( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );
		$sql = "SELECT * FROM user WHERE user_nickname LIKE '%{$key}%'";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function search_column_result( $key ) {
		$key = mysql_real_escape_string( trim( $key ) );
		$sql = "SELECT * FROM topic WHERE topic_title LIKE '%{$key}%'";
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
	
	function html_search_result( $num, $stories, $users, $columns) {
	?>
		<div class="search-result-wrap">
			<div class="search-result-box">
			<?php
				if( $num == 0 ) {
					echo "没有相关的搜索结果。";
				} else {
					if( count( $users ) > 0 ) {
						echo "<div class='result-box'>";
						echo '<h4>相关用户</h4>';
						foreach( $users as $user ) {
							extract( $user);
							$user_photo = UVIEW_PATH.$user_photo;
							$user_link = HOSTURL."user?id={$user_id}";
					?>
						<div class='item'>
							<div class="topic_photo"><a href="<?=$user_link?>" target="_blank"><img src="<?=$user_photo?>" width="40px"/></a></div>
							<div class="topic_link"><a href="<?=$user_link?>" target="_blank"><?=$user_nickname?></a></div>	
						</div>
					<?php
						}
						echo "</div>";
					}
					
					if( count( $columns ) > 0 ) {
						echo "<div class='result-box'>";
						echo '<p>相关话题</p>';
						foreach( $columns as $column ) {
							extract( $column);
							$topic_icon = CVIEW_PATH.$topic_icon;
					?>
						<div class='item'>
							<div class="topic_photo"><a href="<?=HOSTURL.'column?id='.$topic_id?>" target="_blank"><img src="<?=$topic_icon?>" width="40px"/></a></div>
							<div class="topic_link"><a href="<?=HOSTURL.'column?id='.$topic_id?>" target="_blank"><?=$topic_title?></a></div>
						</div>
					<?php
						}
						echo "</div>";
					}
					
					if( count( $stories ) > 0 ) {
						echo "<div class='result-box'>";
						echo '<p>相关故事</p>';
						foreach( $stories as $story ) {
							extract( $story);
					?>
						<div class='item'>
							<div class="topic_link"><a href="<?=HOSTURL.'home/reddit?id='.$story_id?>" target="_blank"><?=$story_title?></a></div>
						</div>
					<?php
						}
						echo "</div>";
					}
					
				}
			?>
			</div>
		</div>
	<?php
	}
	
	function notify_msg() {	// 通知
		
	}
	
	

}
?>
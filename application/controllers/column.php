<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Column extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper(array('date','url', 'form'));
		
		$uid_session = $this->session->userdata('uid');
		if( empty( $uid_session ) ) {
			$uid_session = $this->input->cookie('uid');
			if( !empty( $uid_session ) ) 
				$this->session->set_userdata( 'uid', $uid_session );
		}
		$this->uid = empty( $uid_session ) ? -1: $uid_session;
	}
	public function index($id=-1) {
		$this->load->database();
		$this->load->helper(array('date','url'));
		$this->load->library('session');
		$this->load->model('User_model');
		$this->load->model('Column_model');
		$this->load->model('Story_model');
		$this->load->model('Public_model');
		
		$id = $this->input->get('id');
		
		$data['uid'] = $this->uid ;
		$data['cid'] = $id;
		
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		
		$this->load->view('web/column/column_index.php', $data);
	}
	
	function update_pic() {
		// check login ok ?
		$this->load->model('Column_model');
		$this->load->model('Public_model');
		$this->load->model('User_model');
		$this->load->database();
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->library('session');
		
		$uid = -1;
		$uid = '10000';
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$cid = -1;
		$cid = $this->input->get( 'id' );
		$data['cid'] = $cid;
		$this->session->set_userdata('cid', $cid );
		
		$this->load->view('web/column/edit_column_pic.php', $data);
	}
	
	function chkupload() {
		$this->load->library('form_validation');
		$this->load->helper(array('date','url','form'));
		$this->load->model('Public_model');
		$this->load->model('User_model');
		$this->load->database();
		$this->load->library('session');
	  
		$uid = '10000';
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
	  
		$config['upload_path'] = UPLOAD_PATH. 'topic/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$p= $this->input->post();
		$f = $_FILES['photo'];
	
		$e = $this->Public_model->validate_img( $f, 2 );
		$data['p'] = $p;
		$data['f'] = $f;
		$data['error'] = $e;
		$this->load->view('web/column/upload_success', $data);
	}
	
	function cut() {
		$this->load->model('Public_model');
		$this->load->library('session');
		$this->load->database();
		
		
		$p = $this->input->post();
		$uid = '10000';
		$p['uid'] = $uid;
		$p['cid'] = $this->session->userdata('cid');
		$p['img'] = $this->session->userdata('save_path');
		$p['sw'] = 400;
		$data['cut'] = $p;
		$this->load->view( 'web/column/cut_ok.php', $data );
	}
	
	function seach() {
		$this->load->view( 'web/story/search_index', $data );
	}
	
	
}
?>
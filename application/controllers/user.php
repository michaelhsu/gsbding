<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public $uid;
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url', 'date','cookie'));
		$this->load->library('session');
		$this->load->database();
		$this->load->model('User_model');
		
		$uid_session = $this->session->userdata('uid');
		if( empty( $uid_session ) ) {
			$uid_session = $this->input->cookie('uid');
			if( !empty( $uid_session ) ) 
				$this->session->set_userdata( 'uid', $uid_session );
		}
		$this->uid = empty( $uid_session ) ? -1: $uid_session;
	}

	public function index() {
		$this->load->model('Column_model');
		$this->load->model('Story_model');
		$this->load->model('Public_model');
		$id = -1;
		
		$data['uid'] = $this->uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$id = $this->input->get('id');
		$id = ( empty( $id ) ) ? $data['uid'] : $id;
		$data['author_id'] = $id;
		
		$this->load->view('web/user/user_index.php', $data);
	}
	
	public function editp() {
		$this->load->database();
		$this->load->helper(array('date','url','form'));
		$this->load->library('form_validation');
		
		$type = $this->input->get('type');
		$type = empty( $type ) ? 0 : $type;
		
		$this->load->model('Column_model');
		$this->load->model('Story_model');
		$this->load->model('Public_model');
		if( $type == 0 ) {
			$v = 'web/user/edit_profile.php';
		} else if( $type == 1) {
			$v = 'web/user/edit_photo.php';
		} else if( $type == 2) {
			$v = 'web/user/edit_pass.php';
		}
		// echo form_open(base_url().'home/write', $attrs); 
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid;
		$data['type'] = $type;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$this->load->view($v, $data);
	}
	
	function do_upload() {
		$this->load->library('form_validation');
		$this->load->helper(array('date','url','form'));
		$this->load->model('Public_model');
	  
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
	  
		$config['upload_path'] = UPLOAD_PATH. 'user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$p= $this->input->post();
		$f = $_FILES['photo'];
	
		$e = $this->Public_model->validate_img( $f, 2 );
		$data['p'] = $p;
		$data['f'] = $f;
		$data['error'] = $e;
		$this->load->view('web/user/upload_success', $data);
	}
	
	function cut() {
		$this->load->model('Public_model');
		$p = $this->input->post();
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$p['uid'] = $uid;
		$p['img'] = $this->session->userdata('save_path');
		$p['sw'] = 400;
		$data['cut'] = $p;
		$this->load->view( 'web/user/cut_ok.php', $data );
	}
	
	function follow() {
		$type = $this->input->get('type');
		$this->load->model('Public_model');
		$this->load->model('Story_model');
	
		$data['uid'] = $this->uid;
		
		$id = $this->input->get('id');
		$id = empty( $id ) ? $data['uid'] : $id;
	
		$x = $this->User_model->user_profile( $id );
		$author = $x['row'];
		
		if( $data['uid'] == -1 && ( empty( $id ) || $id == -1 ) ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['type'] = $type;
		$start = 0; $size = 100;
		$r = $this->User_model->get_follow_list( $type, $id, $start, $size );
		if( $type == 0 || $type == 1 ) {
			$r['title'] = $author['user_nickname']."关注的".$r['title'];
		} else if( $type == 2 ) {
			$r['title'] = $author['user_nickname']."的".$r['title'];
		}
		$r['uid'] = $data['uid'];
		$r['type'] = $type;
		$r['xid'] = $id;
		$data['r'] = $r;
		$data['author_id'] = $id;
		
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$this->load->view( 'web/user/follow_index.php', $data );
	}
	
	function chkprof() {
		$this->load->model('Public_model');
		$this->load->library('form_validation');
		
		// $this->form_validation->set_rules('old_pass', '原密码', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		// $this->form_validation->set_rules('new_pass', '新密码', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		// $this->form_validation->set_rules('new_pass2', '密码确认', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		// $this->form_validation->set_rules('story_content', '内容', 'trim|required|min_length[6]');
		// $this->form_validation->set_rules('column_from', '专栏', 'trim|required');
		
		$this->form_validation->set_rules('nickname', '名号', 'trim|required|max_length[20]|xss_clean');
		$uid = $this->uid; 
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid;
		if( $data['uid'] > -1 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
	
		$p = $this->input->post();
		 if ( $this->form_validation->run() == FALSE )	{
			$this->load->view( 'web/user/edit_profile', $data );
		  } else {
			$p = $this->input->post();
			$this->User_model->update_profile( $uid, $p );	// 更新
			redirect( HOSTURL.'user' ); 
		  }
	}
	
	function chkpass() {
		$this->load->model('Public_model');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('old_pass', '原密码', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('new_pass', '新密码', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('new_pass2', '密码确认', 'trim|required|min_length[6]|max_length[20]|xss_clean');
		
		$uid = $this->uid; 
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$x = $this->User_model->user_profile( $uid );
		$data['login'] = $x['row'];
		
		 if ( $this->form_validation->run() == FALSE )	{
			$this->load->view( 'web/user/edit_pass', $data );
		  } else {
			$p = $this->input->post();
			$p['user_pass'] = $login['user_pass'];
			$this->User_model->update_password( $p );	// 更新
			redirect( HOSTURL.'user' ); 
		  }
	}
	
	function history() {
		$this->load->model('Public_model');
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $this->uid;
		$type = $this->input->get('type');
		$type = empty( $type ) ? 0 : $type;
		$start = 0;
		$size = 100;
		$history = $this->User_model->get_read_history( $type, $data['uid'], $start, $size );
		$data['history'] = $history;
		$data['type'] = $type;
		
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$this->load->view('web/user/read_history', $data );	// 装载阅读历史
	}
	
	function top() {
		$this->load->model('Public_model');
		$uid = $this->uid;
		$type = $this->input->get('type');
		$type = empty( $type ) ? 0 : $type;
		$start = 0;
		$size = 1000;
		$count = 0;
		
		$top = $this->Public_model->get_top_list( $type, $count, $start, $size );
		
		$data['top'] = $top;
		$data['type'] = $type;
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$this->load->view('web/user/top_index', $data );	// 装载阅读历史
	}
	
	function editc() {
		$this->load->model('Public_model');
		$this->load->model('Column_model');
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$type = -1;
		$type = $this->input->get( 'type' );
		
		$id = -1;
		$id = $this->input->get('id');
		
		$data['uid'] = $uid;
		$data['type'] = $type;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$this->load->view('web/column/edit_column', $data);	// 装载创建专栏页面
	}
	
	function msg() {
		$this->load->model('Public_model');
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$type = 0;
		$type = $this->input->get('type');
		$type = isset( $type ) ? $type : -1;
		$data['uid'] = $uid;
		$data['type'] = $type;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$this->load->view('web/msg/msg_index', $data );
	}
	
	function write_msg() {
		$this->load->model('Public_model');
		$this->load->model('User_model');
		
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$type = 0; // 0=选择收件人 1=有收件人
		$recv_id = $this->input->get('id');
		$type = $this->input->get('type');
		$receiver = array();
		if( $type == 1 ) {
			$x = $this->User_model->user_profile($recv_id);
			$receiver = $x['row'];
		}
		$data['type'] = $type;
		$data['receiver'] = $receiver;
		$this->load->view('web/msg/write_msg', $data );
	}
	
	function chkmsg() {
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('content', '专栏名', 'trim|required|xss_clean');
		$p = $this->input->post();
		$data['p'] = $p;

		if ( $this->form_validation->run() == FALSE )	{
			$this->load->view( 'web/msg/write_msg', $data );
		  } else {
			$this->User_model->send_msg( $p, 0 );
			$this->load->view( 'web/user/check_profile', $data );
		  }
		
		
	}
	
	function chknewc() {
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$this->load->model('Column_model');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('column_name', '专栏名', 'trim|required|max_length[100]|xss_clean');
		$this->form_validation->set_rules('column_desc', '专栏介绍', 'trim|max_length[140]|xss_clean');
		$p = $this->input->post();
		$data['p'] = $p;
		// $this->load->view( 'web/column/check_profile', $data );
		
		
		 if ( $this->form_validation->run() == FALSE )	{
			$this->load->view( 'web/column/edit_column', $data );
		  } else {
			$p = $this->input->post();
			$p['creator'] = $uid;
			$this->Column_model->new_column( $p );	// 更新
			// redirect( HOSTURL.'user' ); 
			$new_cid = $this->Column_model->get_newly_column_id( $uid );
			$link = HOSTURL."column/update_pic?id={$new_cid}";
			redirect( $link );
		  }
		  
	}
	
	function user_vote() {
		$this->load->model("Public_model");
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid; 
		if( $data['uid'] > -1 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$author_id = $this->input->get( 'id' );
		$data['author_id'] = $author_id;
		$this->load->view( 'web/user/user_vote_index', $data );
	}
	
	function user_comment() {
		$this->load->model("Public_model");
		$uid = $this->uid;
		if( $uid == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		$data['uid'] = $uid; 
		if( $data['uid'] > -1 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$author_id = $this->input->get( 'id' );
		$data['author_id'] = $author_id;
		$this->load->view( 'web/user/user_comment_index', $data );
	}
	
	function user_draft() {
		$this->load->model("Public_model");
		
		$uid = $this->uid;
		$data['uid'] = $uid; 
		if( $data['uid'] > -1 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$author_id = $this->input->get( 'id' );
		$data['author_id'] = $author_id;
		$this->load->view( 'web/user/user_draft_index', $data );
	}
	
	function login() {
		if( isset( $_POST['btn_login'] ) ) {
			$post = $this->input->post();
			$data['p'] = $post;
			$user_email = trim( $post['user_email'] );
			$user_pass = trim( $post['user_pass'] );
			
			$error = $this->User_model->check_login( $user_email, $user_pass );
			$data['error'] = $error;
			if( $error == 0 ) {
				$uid = $this->User_model->query_uid( $user_email );
				$this->session->set_userdata('uid', $uid );
				if( isset( $post['remember'] ) ) {
					$cookie = array(
						'name'   => 'uid',
						'value'  => "{$uid}",
						'expire' => 7*24*3600
					);
					$this->input->set_cookie($cookie);
				}
				$url = base_url()."home";
				redirect( $url );  
			} else {
				$this->load->view('web/user/login', $data);
			}
		} else {
			$this->load->view('web/user/login');
		}	
	}
	
	function register() {
		if( isset( $_POST['btn_reg'] ) ) {
			$post = $this->input->post();
			$data['p'] = $post;
			$error = $this->User_model->check_register( $post );
			$data['error'] = $error;
			if( $error == 0 ) {
				$user_email = $post['user_email'];
				$uid = $this->User_model->query_uid( $user_email );
				$this->User_model->change_follow(0, 0, $uid, '10002');
				$this->User_model->change_follow(0, 0, $uid, '10017');
				$this->User_model->change_follow(0, 0, $uid, '10036');
				$this->session->set_userdata('uid', $uid );
				$cookie = array(
					'name'   => 'uid',
					'value'  => "{$uid}",
					'expire' => 7*24*3600
				);
				$this->input->set_cookie($cookie);
				// 添加默认的关注的对象
				
				$url = base_url()."home";
				redirect( $url );  
			}	else {
				$this->load->view('web/user/register', $data);
			}		
		} else {
			$this->load->view('web/user/register');
		}
	}
	
	function logout() {
		delete_cookie( 'uid' );
		$this->session->unset_userdata('uid');
		$url = HOSTURL."user/login";
		redirect( $url );
	}
	
}
?>
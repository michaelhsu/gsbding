<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cff3fe57a9f0d38eb1ba9a70826b13ea extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url', 'date','cookie'));
		$this->load->library('session');
		$this->load->database();
		//$this->load->model('User_model');
	}
	
	function index() {
		$this->load->model('Admin_model');
		$this->load->model('Public_model');
		
		$type = $this->input->get( 'type' );
		if( empty( $type ) ) $type = 0;
		$data['type'] = $type;
		$this->load->view('web/admin/admin_index', $data);
	}
	
	function login() {
		$this->load->view('web/admin/login');
	}
	
	function edit_story() {
		$this->load->model( 'Admin_model' );
		$this->load->model( 'Story_model' );
		
		$act = $this->input->get( 'act' );
		if( empty( $act ) ) $act = 0;
		$data['act'] = $act;
		$data['story_id'] = -1;
		if( $act == 1 ) {
			$story_id = $this->input->get( 'id' );
			$data['story_id'] = $story_id;
		} 
		
		if( isset( $_POST['btn_post'] )  || isset( $_POST['btn_edit_done'] ) ) {
			$p = $this->input->post();
			$data['p'] = $p;

			$this->Admin_model->post_story( $act, $p );
			if( $act == 0 ) {
				$story_id = $this->db->insert_id();
			} else if( $act == 1 ) {
				$story_id = $p['story_id'];
			}
			$url = HOSTURL."home/reddit?id={$story_id}";
			redirect( $url );	// ��ת
		
		} else {
			$this->load->view('web/admin/edit_story', $data);
		}
	}
}
?>
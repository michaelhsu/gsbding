<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public $uid;
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper(array('date','url', 'form'));
		$this->load->model('User_model');
		
		$uid_session = $this->session->userdata('uid');
		if( empty( $uid_session ) ) {
			$uid_session = $this->input->cookie('uid');
			if( !empty( $uid_session ) ) 
				$this->session->set_userdata( 'uid', $uid_session );
		}
		$this->uid = empty( $uid_session ) ? -1: $uid_session;
	}
	
	public function index() {	// 首页
		$this->load->model('Column_model');
		$this->load->model('Story_model');
		$this->load->model('Public_model');

		$mod = $this->input->get('mod');
		$mod = empty( $mod ) ? 0 : $mod;
		
		$dis_type = $this->input->get('disType');
		$dis_type = empty( $dis_type ) ? 0 : $dis_type;
		
		$data['uid'] = $this->uid;
		$data['mod'] = $mod;
		$data['dis_type'] = $dis_type;
		
		if( $data['uid'] > 0 ) {
			$this->User_model->set_uid( $this->uid );
			$this->User_model->update_feed_table($mod);
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		$this->load->view('web/story/story_index', $data);
	}
	
	function reddit() {	// 阅读首页
		$this->load->helper(array('date','url'));
		$this->load->model('Story_model');
		$this->load->model('Public_model');
		$this->load->model('Column_model');
		$this->load->database();
		$sid = $this->input->get('id');
	
		$s = $this->Story_model->get_story($sid); 
		if( $s['story_type']  == 1 ) {
			$url = HOSTURL."user/?id={$s['user_id']}";
			redirect( $url );
		}
		$data['s'] = $s;
		
		$data['author_id'] = $s['user_id'];
		
		$data['uid'] = $this->uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$this->load->view('web/story/read_it', $data);
	}
	
	function write() {
		$this->load->helper(array('date','url', 'form'));
		$this->load->library('form_validation');
		$this->load->model('Story_model');
		$this->load->model('Public_model');
		
		$sid = $this->input->get('id');
		$act = $this->input->get('act');
	
		$data['uid'] = $this->uid;
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		if( $data['uid'] == -1 ) {
			$url = HOSTURL."user/login";
			redirect( $url );
		}
		
		$s['uid'] = $data['uid'];
		$s['story_id'] = $sid;
		$this->form_validation->set_rules('story_title', '标题', 'trim|required|min_length[2]|max_length[200]|xss_clean');
		$this->form_validation->set_rules('story_content', '内容', 'trim|required');
		$this->form_validation->set_rules('column_from', '专栏', 'trim|required');
		
		$data['s'] = $s;
		$data['act'] = $act;
		
		
		
		 if ($this->form_validation->run() == FALSE)	{
			$this->session->set_userdata('posted', 0 );
			$data['p'] = $this->input->post();
			$this->load->view( 'web/story/write', $data );
		  } else  {
			if( $this->session->userdata('posted') == 0 ) {
				$post = $this->input->post();
				$post['uid'] = $data['uid'];
				$data['p'] = $post;
				$data['id'] = $this->Story_model->post_story( $act, $post );
				$this->session->set_userdata('posted', 0 );
				// $this->load->view('web/story/write_check', $data);
				if( $act == 0 ) {
					$sid = $this->Story_model->get_user_max_sid( $data['uid'] );
				} else if( $act == 1 ) {
					$sid = $post['story_id'];
				}
				$surl = base_url()."home/reddit?id={$sid}";
				redirect( $surl );
			} 
		  }
	}
	
	function rands() {
		$this->load->model('Story_model');
		$sql = "
			SELECT MIN(story_id) AS story_id
			FROM story
		";
		
		$query = $this->db->query($sql);
		$min = $query->row()->story_id;
		
		$sql = "
			SELECT MAX(story_id) AS story_id
			FROM story
		";
		$query = $this->db->query($sql);
		$max= $query->row()->story_id;
		
		$sid = rand($min, $max );
		while( !$this->Story_model->check_story_id( $sid ) ) {
			$sid = rand($min, $max );
		}
		redirect( base_url().'home/reddit?id='.$sid );
	}
	
	function search() {
		$this->load->model( 'Public_model' );
		$data['uid'] = $this->uid;
		$key = $this->input->get( 'key' );
		
		if( $data['uid'] > 0 ) {
			$x = $this->User_model->user_profile($data['uid']);
			$data['login'] = $x['row'];
		}
		
		$r1 = $this->Public_model->search_story_result( $key );
		$r2 = $this->Public_model->search_user_result( $key );
		$r3 = $this->Public_model->search_column_result( $key );
		
		$search_num = count( $r1) + count( $r2 ) + count( $r3 );
		$data['search_num'] = $search_num;
		$data['r1'] = $r1;
		$data['r2'] = $r2;
		$data['r3'] = $r3;
		
		$this->load->view( 'web/story/search_index', $data );
	}
	
}
?>
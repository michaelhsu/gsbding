<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper(array('date','url', 'form'));
		
		$uid_session = $this->session->userdata('uid');
		if( empty( $uid_session ) ) {
			$uid_session = $this->input->cookie('uid');
			if( !empty( $uid_session ) ) 
				$this->session->set_userdata( 'uid', $uid_session );
		}
		$this->uid = empty( $uid_session ) ? -1: $uid_session;
	}
	
	function expand_full_story($id) {
		$this->load->library('session'); 
		$this->load->database();
		$uid = $this->session->userdata('uid');
		$query = $this->db->where('story_id', $id)->get('story');
		$content = $query->row()->story_content;
		echo stripslashes($content);
	}

	
	function vote_story() {
		
		$this->load->model('Story_model');
		
		$story_id = $this->input->get('story_id');
		$type = $this->input->get('type');
		$uid = $this->uid ;
		if( $uid > 9999 ) {
			$flag = $this->Story_model->change_user_vote_status( $uid, $story_id, $type );
			
			$result['status'] = $flag;
			$result['likes'] = $this->Story_model->get_likes_num( $story_id );
			$result['unlikes'] = $this->Story_model->get_likes_num( $story_id, 2 );
			
			echo json_encode( $result );
		}
	}
	
	function vote_comment() {
		
		$this->load->model('Story_model');
		
		$comment_id = $this->input->get('comment_id');
		$voted = $this->input->get('voted');
		$uid = $this->uid ;
		if( $uid > 9999 ) {
			if( $voted == 0 ) {
				$flag = $this->Story_model->change_user_vote_status( $uid, $comment_id, 1, 1 );
			} else {
				$flag = $this->Story_model->del_user_comment_vote( $comment_id, $uid );
			}
			$result['status'] = $flag;
			$result['vote_num'] = $this->Story_model->get_comment_likes_num( $comment_id );
			
			echo json_encode( $result );
		}
		
	}

	public function eat_bding() {
		$this->load->model('Story_model');
		$story_id = $this->input->get('story_id');
		$uid = $this->uid;	
		$this->Story_model->update_view_count( $story_id, $uid );
		if( $uid > 9999 ) {
			$this->Story_model->insert_update_eat_history( $uid, $story_id );
		}
	}
	
	public function del_bding() {
		$this->load->model('Story_model');	
		$story_id = $this->input->get('story_id');
		$uid = $this->uid;
		
		$status =  $this->Story_model->is_exist_story( $story_id, $uid );
		if( $status == 1 ) {
			$this->Story_model->del_story( $story_id );
		}
		echo $status;
	}
	
	
	public function ajax_user_profile() {	// 牛逼的ajax技术
		$this->load->model('User_model');
		$p = $this->input->get('p');
		$size = 100;
		$start = ($p - 1) * $size;
		$this->User_model->set_uid('10000');
		$data = $this->User_model->user_profile();
		echo json_encode( $data );
	}
	
	public function ajax_fetch_stories() {
		$this->load->model('User_model');
		$this->User_model->set_uid('10000');
		$data = $this->User_model->fetch_feed_stories();
		echo json_encode( $data );
	}
	
	public function follow() {
		$this->load->model('User_model');
		
		$type = $this->input->get('type');
		// $type = empty( $type ) ? -1: $type;
		
		$action = $this->input->get('action');
		// $action = empty( $action ) ? -1: $action;
		
		$xid = $this->input->get('id');
		// $xid = empty( $xid ) ? -1: $xid;
		
		$uid = $this->uid;	// 应该从session中获得
		if( $uid > 9999 ) {
			if( $this->User_model->change_follow($type, $action, $uid, $xid) ) {
				if( $action == 1 ) {
					$this->User_model->del_feeds( $type, $uid, $xid );
				} 
				echo 1;
				
			} else {
				echo 0;
			}
		}		
	}
	
	function change_user_page() {
		$this->load->model('User_model');
		$this->load->model('Public_model');
		$this->load->model('Story_model');
		$this->load->helper('url');
		
		$author_id = $this->input->get('author_id');
		$page = $this->input->get('page');
		$dis_type = $this->input->get('dis_type');
		$uid = $this->uid ;	// tbe
		echo $this->User_model->html_user_stories_wrap( $page, $uid, $author_id, $dis_type); 
	}
	
	function change_column_page() {
		$this->load->model('Column_model');
		$this->load->model('Public_model');
		$this->load->model('Story_model');
		$this->load->helper('url');
		
		$column_id = $this->input->get('column_id');
		$page = $this->input->get('page');
		$dis_type = $this->input->get('dis_type');
		$uid = $this->uid ;	// tbe
		echo $this->Column_model->html_column_stories_wrap( $page, $uid, $column_id, $dis_type); 
	}
	
	function change_home_page() {
		$this->load->model('Column_model');
		$this->load->model('User_model');
		$this->load->model('Public_model');
		$this->load->model('Story_model');
		$this->load->helper('url');
		
		$mod = $this->input->get('mod');
		$page = $this->input->get('page');
		$dis_type = $this->input->get('dis_type');
		$uid = $this->uid ;	// tbe
		echo $this->Public_model->html_feed_stories_list( $page, $uid, $mod, $dis_type); 
	}
	
	function user_index() {
		$this->load->model('Column_model');
		$this->load->model('User_model');
		$this->load->model('Public_model');
		$this->load->model('Story_model');
		$this->load->helper('url');
		
		$uid = $this->uid;
		$type = $this->input->get('type');
		$author_id = $this->input->get('author_id');
		
		switch( $type ) {
			case 0:
				echo $this->User_model->html_user_stories_wrap( 1 , $uid, $author_id, 0);
				break;
			case 1:
				echo $this->User_model->html_author_comments_wrap( 1 , $uid, $author_id );
				break;
			case 2:
				echo $this->User_model->html_author_votes_wrap( 1 , $uid, $author_id );
				break;
			case 3:
				echo $this->User_model->html_author_drafts_wrap( 1 , $uid, $author_id );
				break;
		}
	}
	
	function post_comment() {
		$this->load->model('Story_model');
		$this->load->model('User_model');
	
		$uid = $this->uid ;
		if( $uid > 9999 ) {
			$story_id = $this->input->get('story_id');
			$to = $this->input->get('to');
			$cc = $this->input->get('cc');
			$comment_content = $_POST['comment_content'];
			echo $this->Story_model->post_comment( $story_id, $uid, $comment_content );
			$comment_id = $this->Story_model->get_max_comment_id( $story_id, $uid );
			
			
			if( $comment_id > 0 ) {
				if( $to > 9999 && $to != $uid ) 
					$this->Story_model->send_comment_notice( $comment_id, $uid, $story_id, $to );	// 给原作者的
				
				if( $cc > 9999 && $cc != $uid) {
					$this->Story_model->send_comment_notice( $comment_id, $uid, $story_id, $cc );	// 给评论者的
				}
			}
		}
	}
	
	function del_msg() {
		$this->load->model('User_model');
		$msg_id = $this->input->get( 'id' );
		$uid = $this->uid;
		
		echo $this->User_model->del_msg( $msg_id, $uid );
		
	}
	
	function read_msg() {
		$this->load->model('User_model');
		$msg_id = $this->input->get( 'id' );
		$type = $this->input->get( 'type' );
		$uid = $this->uid;
		
		echo $this->User_model->read_msg( $type, $msg_id, $uid );
	}
	
	function search_all() {	//搜索所有相关内容
		$this->load->model('Public_model');
		$key = $_GET[ 'term' ];
		
		$a1 = $this->Public_model->search_story_label( $key );
		$a2 = $this->Public_model->search_user_label( $key );
		$a3 = $this->Public_model->search_column_label( $key );
	
		$a = array_merge( $a1, $a2, $a3 );
		echo json_encode( $a );
	}
	
	function fake_del_bding() {
		$this->load->model( 'Admin_model' );
		
		$story_id = $this->input->get( 'id' );
		echo $this->Admin_model->fake_del_bding( $story_id );	
	}
	
	
}
?>
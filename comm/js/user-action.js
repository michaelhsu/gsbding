$( function() {
	// 1
	var base_url = "http://www.gsbding.com/";	// tbe
	
	function fetchStory( idx, elem ) {
		$(elem).click( function() {
			var storyId = $(elem).attr( 'rel' );
			// alert( storyId );
			$.ajax({
				url: base_url + "ajax/expand_full_story/" + storyId,
				success: function( data ) {
					$(elem).parent().html( data );	// 输出故事内容
					// alert( data );
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
			
			$.ajax({	// 更新一下阅读记录和排行榜
				url: base_url + "ajax/eat_bding?story_id=" + storyId,
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});	// end of $.ajax
		} );
	}
	
	// 2	
	function expandComment( idx, elem ) {
		$(elem).click( function() {
			var storyId = $(elem).attr('rel');
			// alert( storyId );
			$('#comment-field-'+storyId).fadeToggle();
		} );
	}
	// 抓取完整文章
	var fetchStoryLinks = $( 'a[action="fetch-story"]' );
	$.each( fetchStoryLinks, fetchStory);
	
	// 展开评论区域
	var expandCommentLinks = $( 'a[action="expand-comment"]' );
	$.each( expandCommentLinks, expandComment);
	
	// 投票按钮的实现	
	var likeBtns = $("a[action='like-story']");
	$.each( likeBtns, function(idx, elem) {
		var marked = $(elem).attr('marked');
		var storyId = $(elem).attr('rel');
		if( marked == 1) {
			$(elem).addClass('disable');
		}
		
		$(elem).click( function() {
			marked = $(elem).attr('marked');	
			if( marked == 0 ) {
				// 更改本按钮
				$.ajax({
					url: base_url + "ajax/vote_story?story_id="+storyId+"&type=1",
					dataType: 'json',
					success: function( data ) {
						if( parseInt(data.status) == 1) {
							$("#like-story-"+storyId).attr('marked', 1);
							$("#like-story-"+storyId).addClass('disable');
							
							$("#unlike-story-"+storyId).attr('marked', 0);
							$("#unlike-story-"+storyId).removeClass('disable');
							
							$("#likes-sta-"+storyId).text( '('+ data.likes +')' );
							$("#unlikes-sta-"+storyId).text('('+ data.unlikes+')' );
						}
					},
					beforeSend: function() {
						$('#loading').show();
					},
					complete: function() {
						$('#loading').hide();
					}
					
				});
				
			}
		});
		
	} );
	
	var unlikeBtns = $('a[action="unlike-story"]');
	$.each( unlikeBtns, function(idx, elem) {
		var marked = $(elem).attr('marked');	// 1投票不喜欢
		var storyId = $(elem).attr('rel');
		var type = $(elem).attr('data');
		if( marked == 1 ) {
			$(elem).addClass('disable');
		}
		
		$(elem).click( function() {
			marked = $(elem).attr('marked');
			if( marked == 0 ) {
				$.ajax({
					url: base_url + "ajax/vote_story?story_id="+storyId+"&type=2",
					dataType: 'json',
					success: function( data ) {
						if( parseInt(data.status) == 1) {
							$("#unlike-story-"+storyId).attr('marked', 1);
							$("#unlike-story-"+storyId).addClass('disable');
							
							$("#like-story-"+storyId).attr('marked', 0);
							$("#like-story-"+storyId).removeClass('disable');
							
							$("#likes-sta-"+storyId).text( '('+ data.likes +')' );
							$("#unlikes-sta-"+storyId).text('('+ data.unlikes+')' );
						}
					},
					beforeSend: function() {
						$('#loading').show();
					},
					complete: function() {
						$('#loading').hide();
					}
				});
			}
		});
	// end of $();
	} );
	
	// 评论功能
	var postComments = $("a[action='post-comment']");
	$.each(postComments, function(idx, elem) {
		$(elem).click(function() {
			var storyId = $(elem).attr('rel');
			var cc = $(elem).attr('cc');
			var to = $(elem).attr('to');
			var content = $(elem).parent().prev().val().trim();	// 定位到textarea
			// alert( content );
			if( content != "" ) {
				$.ajax({
					type: 'POST',
					url: base_url + "ajax/post_comment?story_id="+storyId+"&cc="+cc+"&to="+to,
					dateType: 'html',
					data: {
						comment_content: content
					},
					beforeSend: function() {
						$('#loading').show();
					},
					success: function(data) {
						if( data != "" ) {
							var firstCommentItem = $('#comment-field-'+storyId+' .comment-list '+' .comment-item').first();
							firstCommentItem.after( data );
							$("#comment-field-"+storyId+" textarea").val("");
						}
					},
					complete: function() {
						$('#loading').hide();
					}
				});
		} else {
			alert("评论不能为空!");
		}
		// 
		});
	});
	
	// 阅读按钮
	var bdings = $("a[action='eat-bding']");
	$.each( bdings, function(idx, elem) {
		$(elem).click( function() {
			var storyId = $(elem).attr( 'rel' );
			$.ajax({	
				url: base_url + "ajax/eat_bding?story_id="+storyId
			});	// end of $.ajax
		} );
	} );
	
	// 删除按钮
	function createDelConfirmDialog( id ) {
		var overlay = $("<div class='overlay'></div>");
		var wrap = $("<div  class='dialog-wrap' id='" + id + "'></div>");
		overlay.appendTo("html>body");
		
		var title = $("<div class='dialog-title'>").append($("<span></span>").text("删除故事"));
		title.appendTo( wrap );
		
		var box = $("<div class='dialog-box'>");
		box.appendTo( wrap );
		
		var content = $("<div class='dialog-content'>").text("您确定要删除这个故事吗?");
		content.appendTo( box );
		
		var btns = $("<div class='dialog-btn'></div>");
		var btn1= $("<a href='javascript:void(0);' class='btn btn-success btn-sm' style='margin-right: 20px;'>确定</a>");
		var btn2= $("<a href='javascript:void(0);' class='btn btn-success btn-sm'>取消</a>");
		btn1.appendTo( btns ); 
		btn2.appendTo( btns ); 
		btns.appendTo( box );
		wrap.appendTo('html>body');
		
		btn1.click(function() {
			wrap.remove();	//tbd	
			$.ajax({
				url: base_url + "ajax/del_bding?story_id="+id,
				success: function( data ) {
					if( data == 1 ) {
						setTimeout(function() {
							createMsgDialog("删除成功!");
						}, 200);	//加个延迟
						
						$("#story-"+id).remove();	// 移除
					} else if( data == 0 ) {
						createMsgDialog("您没有权限删除该故事!");
					}
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		});
		
		btn2.click(function() {
			wrap.remove();
			overlay.remove();
		});
		
		overlay.click( function() {
			wrap.remove();
			overlay.remove();
		} );
		
		$(window).scroll(function() {
			var left = ($(window).width() - $("#"+id).width()) / 2;
			var top = $(window).scrollTop() + ($(window).height() - $("#"+id).height()) / 2;
			var olTop =  $(window).scrollTop() ;
			wrap.css({
				left: left,
				top: top
			});
			overlay.css({
				left: 0,
				top: olTop
			});
		});
		
		// init
		var left = ($(window).width() - $("#"+id).width()) / 2;
		var top =  $(window).scrollTop() + ($(window).height() - $("#"+id).height()) / 2;
		var olTop =  $(window).scrollTop() ;
		wrap.css({
			left: left,
			top: top
		});
		overlay.css({
			left: 0,
			top: olTop
		});

	}
	
	
	function createMsgDialog( content ) {
		var overlay = $("<div class='overlay'></div>");
		var wrap = $("<div  class='dialog-wrap'></div>");
		var title = $("<div class='dialog-title'>").append($("<span></span>").text("结果"));
		title.appendTo( wrap );
		
		var box = $("<div class='dialog-box'>");
		var content = $("<div class='dialog-content'>").text( content );
		content.appendTo( box );
		var btns = $("<div class='dialog-btn'></div>");
		var btn2= $("<a href='javascript:void(0);' class='btn btn-success btn-sm'>确定</a>");
		btn2.appendTo( btns ); 
		btns.appendTo( box );
		box.appendTo( wrap );
		
		wrap.appendTo('html>body');
		overlay.appendTo("html>body");

		btn2.click(function() {
			wrap.remove();
			overlay.remove();
		});
		
		overlay.click( function() {
			wrap.remove();
			overlay.remove();
		} );
		
		// init
		var left = ($(window).width() - wrap.width()) / 2;
		var top =  $(window).scrollTop() + ($(window).height() - wrap.height()) / 2;
		var olTop =  $(window).scrollTop() ;
		wrap.css({
			left: left,
			top: top
		});
		overlay.css({
			left: 0,
			top: olTop
		});

	}
	
	
	var delList = $("a[action='del-bding']");
	$.each( delList, function(idx, elem) {
		$(elem).click( function() {
			var storyId = $(elem).attr('rel');
			createDelConfirmDialog(storyId);
		} );
	} );
	
	var fakeDelLinks = $("a[action='fake-del-bding']");
	$.each( fakeDelLinks, function( idx, elem ) {
		$(elem).click( function() {
			var storyId = $(elem).attr( 'rel' );
			$.ajax({
				url: base_url + "ajax/fake_del_bding?id="+storyId ,
				success: function( data ) {
					if( parseInt( data ) == 1 ) {
						alert("删除成功！");
					} else if( parseInt( data ) == 0 ) {
						alert("删除失败！");
					}
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		} );
		
	} );
	
	//  更改显示方式
	var changeP = $("a[action='changep']");
	$.each( changeP, function(idx, elem) {
		$(elem).click( function() {
			var authorId = $(elem).attr('rel');
			var page = $(elem).attr('p');
			var disType = $(this).attr('type');
			$.ajax({
				url: base_url + "ajax/change_user_page?author_id="+authorId + "&page="+page+"&dis_type="+disType ,
				success: function( data ) {
					$("#stage").html(data);
					var vPos = $("#public-wrap").offset().top - 120;
					$('html>body').animate({
						scrollTop: vPos
					}, 500);
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		} );
	} );
	
	// 分页 
	var selUserPage = $("#sel-user-page");
	selUserPage.change(function() {
		var page = $(this).val();
		var authorId = $(this).attr('rel');
		var disType = $(this).attr('type');
		$.ajax({
			url: base_url + "ajax/change_user_page?author_id="+authorId + "&page="+page+"&dis_type="+disType ,
			success: function( data ) {
				$("#stage").html(data);
				var vPos = $("#public-wrap").offset().top - 120;
				$('html>body').animate({
					scrollTop: vPos
				}, 500);
			},
			beforeSend: function() {
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		});
	});
	
	var changeColumnP = $("a[action='change-columnp']");
	$.each( changeColumnP, function(idx, elem) {
		$(elem).click( function() {
			var columnId = $(elem).attr('rel');
			var page = $(elem).attr('p');
			var disType = $(this).attr('type');
			$.ajax({
				url: base_url + "ajax/change_column_page?column_id="+columnId + "&page="+page+"&dis_type="+disType ,
				success: function( data ) {
					$("#stage").html(data);
					var vPos = $("#public-wrap").offset().top - 120;
					$('html>body').animate({
						scrollTop: vPos
					}, 500);
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		} );
	} );
	
	var selColumnPage = $("#sel-column-page");
	selColumnPage.change(function() {
		var page = $(this).val();
		var columnId = $(this).attr('rel');
		var disType = $(this).attr('type');
	
		$.ajax({
			url: base_url + "ajax/change_column_page?column_id="+columnId + "&page="+page+"&dis_type="+disType ,
			success: function( data ) {
				$("#stage").html(data);
				var vPos = $("#public-wrap").offset().top - 120;
				$('html>body').animate({
					scrollTop: vPos
				}, 500);
			},
			beforeSend: function() {
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		});
	});
	
	
	var changeHomeP = $("a[action='change-homep']");
	$.each( changeHomeP, function(idx, elem) {
		$(elem).click( function() {
			var page = $(elem).attr('p');
			var disType = $(this).attr('type');
			var mod = $(this).attr('mod');
			$.ajax({
				url: base_url + "ajax/change_home_page?page="+page+"&dis_type="+disType + "&mod="+mod,
				success: function( data ) {
					$("#stage").css({
						'width': '620px',
						'min-height': '800px',
						'margin': '0 auto',
						'margin-right': '10px'
					});
					$("#stage").html(data);
					var vPos = $("#stage").offset().top - 120;
					$('html>body').animate({
						scrollTop: vPos
					}, 500);
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		} );
	} );
	
	var selHomePage = $("#sel-home-page");
	selHomePage.change(function() {
		var page = $(this).val();
		var disType = $(this).attr('type');
		var mod = $(this).attr('mod');
	
		$.ajax({
			url: base_url + "ajax/change_home_page?page="+page+"&dis_type="+disType + "&mod="+mod,
			success: function( data ) {
				$("#stage").css({
					'width': '620px',
					'min-height': '800px',
					'margin': '0 auto',
					'margin-right': '10px'
				});
				$("#stage").html(data);
				var vPos = $("#stage").offset().top - 120;
				$('html>body').animate({
					scrollTop: vPos
				}, 500);
			},
			beforeSend: function() {
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		});
	});
	
	var userIndexActs = $("a[action='user_index']");
	$.each( userIndexActs, function( idx, elem ) {
		$(elem).click( function() {
			$("li.active").removeClass('active');
			var type = $(elem).attr('type');
			var authorId = $(elem).attr('rel');
			$(elem).parent().addClass('active');
			$.ajax({
				url: base_url + "ajax/user_index?author_id="+authorId+"&type="+type,
				success: function( data ) {
					$("#stage").css({
						'width': '620px',
						'min-height': '800px',
						'margin': '0 auto',
						'margin-right': '10px'
					});
					
					$("#stage").html(data);
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		} );
	} );
	
	// 评论操作隐藏
	
	$(".comment-item  .user-action").css("visibility", "hidden");
	
	$(".comment-item").hover( 
		function() {
			$(this).find(".user-action").css("visibility", "visible");
	
		},
		function() {
			 $(this).find(".user-action").css("visibility", "hidden");
	
		}
	);
	
	
		var voteCommentBtns = $("a[action='vote-comment']");
		$.each( voteCommentBtns, function(idx, elem) {
			$(elem).click( function() {
				var commentId = $(elem).attr("rel");
				var voted = $(elem).attr("voted");
				$.ajax({
					url: base_url + "ajax/vote_comment?comment_id="+commentId+"&voted="+voted,
					dataType: 'json',
					success: function( data ) {
						if( data.status == 1 ) {
							if( voted == 1 ) {
								$(elem).removeClass("voted");
								$(elem).attr( 'voted', 0 );
								$("#comment-vote-"+commentId).text( data.vote_num );
							} else if( voted == 0 ) {
								$(elem).addClass("voted");
								$(elem).attr( 'voted', 1 );
								$("#comment-vote-"+commentId).text( data.vote_num );
							}
						}
					},
					beforeSend: function() {
						$('#loading').show();
					},
					complete: function() {
						$('#loading').hide();
					}
				});
			} );
		});
	
		var replyCommentBtns = $("a[action='reply-comment']");
		$.each( replyCommentBtns, function(idx, elem) {
			$(elem).click( function() {
				var commentId = $(elem).attr("rel");
				var name = $("#comment-"+commentId+ " .name > a").text();
				var storyId = $(elem).attr("tar");
				var comment_content = $("#comment-"+commentId).find(".say-words").text();
				// alert( comment_content );	// tbd
				
				var topVal = $( "#comment-field-"+storyId ).offset().top - 130;
				
				$('html>body').animate({
					scrollTop: topVal
				}, 500);
				$( "#comment-field-"+storyId+" textarea" ).css({border: "1px solid #bc0000"});
				var ccVal = $(elem).attr('cc');	// copy to user
				$( "#comment-field-"+storyId+" .post-comment-btn" ).attr('cc', ccVal);	// 添加评论抄送
				comment_content = '//@' + name+':'+comment_content.trim()
				$("#comment-field-"+storyId+ " textarea").val("");
				$("#comment-field-"+storyId+ " textarea").val( comment_content.trim() );
			} );
	});
	
	var msgLinks = $("a[action='read-msg']");
	$.each(msgLinks, function(idx, elem) {	
		$(elem).click( function() {
			var mailId = $(elem).attr('rel');
			var status = $(elem).attr( 'status' );
			var type = $(elem).attr('type');
			if( parseInt( status ) == 0 ) {
				$(elem).attr('status', 1);
				$("#msg-"+mailId).removeClass('bold');
				$("#msg-status-"+mailId).text('已读');
				$.ajax({
					url: base_url+"ajax/read_msg?id="+mailId+"&type="+type
				});
			}
			$('#reply-msg-'+mailId).fadeToggle();
		
		} );
	});
	
	var delMsgLinks = $("a[action='del-msg']");
	$.each( delMsgLinks, function( idx, elem ) {
		$(elem).click(function() {
			var mailId = $(elem).attr( "rel" );
			$("#msg-"+mailId).remove();
			$.ajax({
				url: base_url + "ajax/del_msg?id=" + mailId,
				success: function( data ) {
					if( parseInt( data ) == 0 ) {
						alert( "删除错误！" );
					}
				},
				beforeSend: function() {
					$('#loading').show();
				},
				complete: function() {
					$('#loading').hide();
				}
			});
		});
	} );
	
	
	
	
	
	
	
	
	
	
	
} );